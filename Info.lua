g_PluginInfo = {
  Name = "SkyWars",
  Version = "20160112",
  Date = "2016-01-12",
  Description = 'SkyWars',

  ConsoleCommands =
  {
  },

--------------------------------------------------------------------------------

  Commands = {
    ['/SkyWars'] = {
      Alias = {'/sw'},
      HelpString = "Sky wars plugin",
      Permission = 'skywars.core',

      Subcommands = {

        request = {
          Alias = {'r'},
          Handler = commandGUIRequest,
        }, -- request

        pc = {
          HelpString = 'Set players count to start',
          Permission = 'skywars.build',
          Handler = commandGUIPlayersCountSet,
        },

        mark = {
          Alias = {'m'},
          Handler = commandGUIMark,
          Permission = 'skywars.build',
        }, -- mark

        save = {
          Alias = {'s'},
          Handler = commandGUISave,
          Permission = 'skywars.build',
        },  -- save

        load = {
          Alias = {'l'},
          Handler = commandGUILoad,
          Permission = 'skywars.build',
        },

        exit = {
          Alias = {'e'},
          Handler = commandGUIExit,
        },

        spawn = {
          Alias = {'sp'},
          Permission = 'skywars.build',
          Subcommands = {
            -- set = {
            --   Alias = {'s'},
            --   Handler = commandGUISpawnSet,
            -- },

            mark = {
              Alias = {'m'},
              Handler = commandGUISpawnMark,
            },

            list = {
              Alias = {'l'},
              Handler = commandGUISpawnList,
            },

            del = {
              Alias = {'d'},
              Handler = commandGUISpawnDel,
            }, -- del

            tp = {
              Handler = commandGUISpawnTP,
            }, -- tp
          }, -- Subcommands
        }, -- spawn
      },  -- Subcommands
    },  -- /SkyWars
  },  -- Commands

--------------------------------------------------------------------------------

} -- g_PluginInfo

