-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- commandGUIRequest()
--
-------------------------------------------------------------------------------

function OnPlayerBreakingBlock(aPlayer, aBlockX, aBlockY, aBlockZ, aBlockFace, aBlockType, aBlockMeta)

  local func_name = 'OnPlayerBreakingBlock()';

  local player = gPlayers[aPlayer:GetUniqueID()];

  -- Is it spawn mark?
  if player == nil then
    return false;
  end

  if player:getSpawnMark() == true then
    -- There are spawn mark
    if player:spawnMarkSave(Vector3i(aBlockX, aBlockY, aBlockZ)) == true then
      aPlayer:SendMessageSuccess(msgSpawnSetSuccess);
    else
      aPlayer:SendMessageWarning(msgMarkPointsNotSet);
    end

    -- Turn off mark
    player:setSpawnMark(false);

    -- Disable block break
    return true;
  end

  -- Allow other plugins
  return false;
end
-------------------------------------------------------------------------------

