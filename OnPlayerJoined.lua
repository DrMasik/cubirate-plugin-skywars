-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- OnPlayerJoined()
--
--------------------------------------------------------------------------------

function OnPlayerJoined(aPlayer)

  local func_name = 'OnPlayerJoined()';

  -- Create flag of player join
  gPlayerJoin[aPlayer:GetName()] = 1;

  -- Allow other plugins
  return false;
end

