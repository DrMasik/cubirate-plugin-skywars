-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- OnPlayerMoving()
--
-------------------------------------------------------------------------------

function OnPlayerMoving(aPlayer, aOldPosition, aNewPosition)

  local func_name = 'OnPlayerMoving()';

  -- Protect map from evil gamers

  -- Get player class object for the player
  local player = gPlayers[aPlayer:GetUniqueID()];

  -- Is it for the plugin?
  if player == nil then
    return false;
  end

  -- Is it player in the map - cuboid object exists
  local mapCuboid = player:getCuboid();
  if mapCuboid == nil then
    return false;
  end

  -- Check player die
  if mapCuboid:IsInside(aPlayer:GetPosition()) == true then
    return false;
  end

  -- Set fly mode for protect player
  aPlayer:SetFlying(true);

  -- Player die
  player:loseGame();

  return false;
end
-------------------------------------------------------------------------------
