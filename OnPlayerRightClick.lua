-- =============================================================================
--
-- SkyWars
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- OnPlayerRightClick()
--
--------------------------------------------------------------------------------

function OnPlayerRightClick(aPlayer, aBlockX, aBlockY, aBlockZ, BlockFace, CursorX, CursorY, CursorZ)

  local func_name = "OnPlayerRightClick()";

  -- local blockCoordinates = ";".. aBlockX ..";".. aBlockY ..";".. aBlockZ ..";";

  local world = aPlayer:GetWorld();

  -- Get block type
  local isSign, r1, r2, r3, r4 = world:GetSignLines(aBlockX, aBlockY, aBlockZ);

  if not isSign then
    return false;
  end

  -- Check fields
  if r1 == nil or r2 == nil or r3 == nil or r4 == nil then
    return false;
  end

  -- Is it processed sign?
  if r1:lower() ~= '[skywars]' then
    return false;
  end

  local arenaName = tostring(r2);

  -- Is it arena exists?
  if gStore:arenaExists(arenaName) == false then
    aPlayer:SendMessageWarning(msgArenaWrongName);
    return true;
  end

  -- Is it arena active?
  if gStore:arenaActive(arenaName) == false then
    aPlayer:SendMessageWarning(msgArenaTurnedOff);
    return true;
  end

  local Split = {'/sw', 'r', arenaName};

  commandGUIRequest(Split, aPlayer);

  return true;
end

--------------------------------------------------------------------------------

