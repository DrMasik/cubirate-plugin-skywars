-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- OnPlayerSpawned()
--
--------------------------------------------------------------------------------

function OnPlayerSpawned(aPlayer)

  local func_name = 'OnPlayerSpawned()';

  -- gSystem:console_log('Begin function', 1, func_name);

  -- Is it player inventory needed to restore
  if gPlayerJoin[aPlayer:GetName()] == nil then
    -- gSystem:console_log('gPlayerJoin[aPlayer:GetName()] == nil', 1, func_name);
    return false;
  end

  -- Clean restore flag
  gPlayerJoin[aPlayer:GetName()] = nil;

  -- gSystem:console_log('gPlayerJoin[aPlayer:GetName()] ~= nil', 1, func_name);

  ----------------------------------
  -- Restore player data
  ----------------------------------

  -- Create player class object
  local playerCurr = cPlayers:new();

  playerCurr:setPlayerName(aPlayer:GetName());
  playerCurr:setPlayerUUID(aPlayer:GetUUID());

  gPlayersRestoreData[aPlayer:GetUUID()] = playerCurr;

  -- Allow other plugins
  return false;
end

