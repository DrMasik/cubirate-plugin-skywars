-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:Destroy()
--
-------------------------------------------------------------------------------

function cArenaInstances:Destroy()
  -- Destructor

  local func_name = 'cArenaInstances:Destroy()';

  -- gSystem:console_log('Delete map instance ID = '.. self.arenaInstanceID, 1, func_name);

  gArenaInstances[self.arenaInstanceID] = nil;
  -- self = nil;

end
-------------------------------------------------------------------------------

