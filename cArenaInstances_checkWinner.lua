-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:checkWinner()
--
-------------------------------------------------------------------------------
function cArenaInstances:checkWinner()
  local func_name = 'cArenaInstances:checkWinner()';

  local plUUID;
  local playersLeftCount = self:getPlayersLeftUUIDCount();

  -- gSystem:console_log('#self.playersLeftUUID = '.. #self.playersLeftUUID, 1, func_name);

  -- Check player count
  if playersLeftCount > 1 then
    return true;
  end

  -- There are last player disconnect (may error)
  if playersLeftCount == 0 then
    -- Clean world from map instance
    -- self:cleanWorld();
    -- self:Destroy();
    return true;
  end

  -- Get last user UUID
  for uuid in pairs(self.playersLeftUUID) do
    plUUID = uuid;
  end

  local player;

  -- Get player object
  cRoot:Get():DoWithPlayerByUUID(plUUID,
    function(aPlayer1)
      player = aPlayer1;
      aPlayer1:GetInventory():Clear();
    end
  );

  -- Check. Simple check
  if player == nil then
    gSystem:console_log('player is nil', 2, func_name);
    return false;
  end

  local playerCurr = gPlayers[player:GetUniqueID()];

  -- Reset player cuboid. Hi is winner
  playerCurr:setCuboid(nil);

  -- Teleport player to last position
  playerCurr:teleportBack();

  -- Send win message to player
  player:SendMessageSuccess(msgYouAreWinner);

  -- Retore game mode
  playerCurr:restoreGameMode();

  -- Restore fly permission
  playerCurr:restoreFlyPermission();

  -- Restore flying
  playerCurr:restoreFlying();

  -- Restore player inventory
  playerCurr:restoreInventory();

  -- Restore health level
  playerCurr:restoreHealth();

  -- Restore food level
  playerCurr:restoreFoodLevel();

  -- Restore expirience level
  playerCurr:restoreExpirienceLevel();

  -- Delete from arena the player UUID
  self.playersLeftUUID[plUUID] = nil;

  return true;
end

