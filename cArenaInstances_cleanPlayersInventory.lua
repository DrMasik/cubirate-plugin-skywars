-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:cleanPlayersInventory()
--
-------------------------------------------------------------------------------

function cArenaInstances:cleanPlayersInventory()

  local func_name = 'cArenaInstances:cleanPlayersInventory()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        gPlayers[aPlayer1:GetUniqueID()]:cleanInventory();
      end
    );
  end

end
-------------------------------------------------------------------------------

