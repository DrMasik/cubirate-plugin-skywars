-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:cleanWorld()
--
-------------------------------------------------------------------------------

function cArenaInstances:cleanWorld()

  local func_name = 'cArenaInstances:cleanWorld()';

  -- self.mapBlock = {}; -- cBlockArea loaded from DB
  -- self.mapCuboid = {};
  -- self.playersUUID = {};  -- List of players UUID request
  -- self.worldPosition;

  -- Is it map was written into world?
  if self.worldPosition.x == nil or self.worldPosition.y == nil or self.worldPosition.z == nil then
    -- gSystem:console_log('self.worldPosition.x == nil or self.worldPosition.y == nil or self.worldPosition.z == nil', 1, func_name);
    return true;
  end

  local block = cBlockArea();
  local world = cRoot:Get():GetDefaultWorld();
  local mapSize = Vector3i(self.mapBlock:GetSize());

  -- Load current map
  -- block:LoadFromSchematicString(self.mapBlock);

  -- Create empty block of air
  -- block:Create(mapSize.x, mapSize.y, mapSize.z);

  -- Fill out by air
  -- block:Fill(cBlockArea.baTypes + cBlockArea.baMetas, E_BLOCK_AIR);

  -- Write air block to world
 --  block:Write(world, self.worldPosition, cBlockArea.baTypes + cBlockArea.baMetas);


  local x, y, z;
  local tmp;

  x = 0;
  while x <= mapSize.x do

    z = 0;
    while z <= mapSize.z do

      y = 0;
      while y <= mapSize.y do
        tmp = world:GetBlock(self.worldPosition.x + x, self.worldPosition.y + y, self.worldPosition.z + z);
        -- if tmp ~= E_BLOCK_AIR and tmp ~= E_BLOCK_GRASS then
        -- clean furnace items
        if tmp == E_BLOCK_FURNACE then
          world:DoWithFurnaceAt(self.worldPosition.x + x, self.worldPosition.y + y, self.worldPosition.z + z,
            function(aFurnace)
              aFurnace:GetContents():Clear();
            end
          );
        end

        -- Clean chest items
        if tmp == E_BLOCK_CHEST then
          world:DoWithChestAt(self.worldPosition.x + x, self.worldPosition.y + y, self.worldPosition.z + z,
            function(aChest)
              aChest:GetContents():Clear();
            end
          );
        end

        -- Clean block
        world:DigBlock(self.worldPosition.x + x, self.worldPosition.y + y, self.worldPosition.z + z);

        y = y + 1;
      end

      z = z + 1;
    end

    x = x + 1;
  end

  return true;
end
-------------------------------------------------------------------------------

