-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:createCuboid()
--
-------------------------------------------------------------------------------

function cArenaInstances:createCuboid()

  local func_name = 'cArenaInstances:createCuboid()';
  local mapSize = Vector3i(self.mapBlock:GetSize());

  local mapCuboid = cCuboid(
    Vector3i(self.worldPosition.x, self.worldPosition.y, self.worldPosition.z),
    Vector3i(self.worldPosition.x + mapSize.x, self.worldPosition.y + mapSize.y, self.worldPosition.z + mapSize.z)
  );

  -- Sort for better perfomance
  mapCuboid:Sort();

  -- Save map cuboid
  self.mapCuboid = mapCuboid;

  return true;
end
-------------------------------------------------------------------------------

