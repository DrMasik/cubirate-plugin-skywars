-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:delPlayersLeftUUID()
--
-------------------------------------------------------------------------------

function cArenaInstances:delPlayersLeftUUID(aUUID)

  self.playersLeftUUID[aUUID] = nil;

end
-------------------------------------------------------------------------------

