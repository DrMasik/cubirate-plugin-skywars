-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:getInstanceSize()
--
-------------------------------------------------------------------------------

function cArenaInstances:getInstanceSize()

  if self.mapBlock == nil then
    return false;
  end

  return Vector3i(self.mapBlock:GetSize());

end
-------------------------------------------------------------------------------

