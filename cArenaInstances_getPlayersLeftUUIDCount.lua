-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:getPlayersLeftUUIDCount()
--
-------------------------------------------------------------------------------

function cArenaInstances:getPlayersLeftUUIDCount()

  local count = 0;

  for id1 in pairs(self.playersLeftUUID) do
    count = count + 1;
  end

  return count;

end
-------------------------------------------------------------------------------

