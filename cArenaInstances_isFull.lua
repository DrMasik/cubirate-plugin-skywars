-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:isFull()
--
-------------------------------------------------------------------------------

function cArenaInstances:isFull()

  local func_name = 'cArenaInstances:isFull()';

  -- gSystem:console_log('#self.mapSpawnPoints = '.. #self.mapSpawnPoints, 1, func_name);
  -- gSystem:console_log('#self.playersUUID = '.. #self.playersUUID, 1, func_name);

  if #self.mapSpawnPoints <= #self.playersUUID then
    return true
  else
    return false;
  end

end
-------------------------------------------------------------------------------

