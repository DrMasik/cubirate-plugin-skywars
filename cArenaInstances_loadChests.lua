-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:loadChests()
--
-------------------------------------------------------------------------------

function cArenaInstances:loadChests()
-- E_BLOCK_CHEST

  local func_name = 'cArenaInstances:loadChests()';

  -- Get chests ID and position from DB for the map ID
  local ret, chests = gStore:getChestsByMapID(self.mapID);
  local chestCurr;

  if ret == false then
    gSystem:console_log(msgChestLoadForMapError, 2, func_name);
    return false;
  end

  local world = cRoot:Get():GetWorld(self.worldName);

  -- Is it world found?
  if world == nil or world == false then
    return false;
  end

  local worldPosition = self.worldPosition;
  local chestItems = {};
  local chestCurrPos = {};
  local ret;
  local chestItemCurr;
  local chestSlot = 0;
  local InstanceBoundingBox  = cBoundingBox(Vector3d(self.mapCuboid.p1), Vector3d(self.mapCuboid.p2));

  -- Process per chest items
  for id1 in pairs(chests) do
    chestSlot = 0;
    chestCurr = chests[id1];

    chestCurrPos = Vector3i(worldPosition.x + chestCurr.x,
                            worldPosition.y + chestCurr.y,
                            worldPosition.z + chestCurr.z
    );

    -- Delete old chest items
    world:DoWithChestAt(chestCurrPos.x, chestCurrPos.y, chestCurrPos.z,
      function(aChest)
        aChest:GetContents():Clear();
        return true;
      end
    );

    -- Dig block
    world:DigBlock(chestCurrPos.x, chestCurrPos.y, chestCurrPos.z);

    -- Set chest block
    world:SetBlock(chestCurrPos.x, chestCurrPos.y, chestCurrPos.z, E_BLOCK_CHEST, 2);

    -- Delete all entities into map cuboid
    world:ForEachEntityInBox(InstanceBoundingBox,
      function (aEntity)
        if aEntity:GetEntityType() == aEntity.etPickup then
          aEntity:Destroy();
        end
      end
    );

    -- Get items for the chest
    ret, chestItems = gStore:getChestItems(chestCurr.id);

    if ret ~= true then
      gSystem:console_log('Con not load chest items for the chest ID ='.. chestCurr.id, 2, func_name);
    end

    world:DoWithChestAt(chestCurrPos.x, chestCurrPos.y, chestCurrPos.z,
      function(aChest)

        -- Fill out chest by items
        for id1 in pairs(chestItems) do
          chestItemCurr = chestItems[id1];

          -- Put item into chest
          aChest:SetSlot(chestSlot, cItem(chestItemCurr.id, chestItemCurr.count, 0, chestItemCurr.enchant));

          -- Get next slot
          chestSlot = chestSlot + 1;
        end
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

