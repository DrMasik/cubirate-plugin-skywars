-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:loadFurnaces()
--
-------------------------------------------------------------------------------

function cArenaInstances:loadFurnaces()
-- E_BLOCK_CHEST

  local func_name = 'cArenaInstances:loadFurnaces()';

  -- Get furnaces ID and position from DB for the map ID
  local ret, furnaces = gStore:getFurnacesByMapID(self.mapID);
  local furnaceCurr;

  if ret == false then
    gSystem:console_log(msgChestLoadForMapError, 2, func_name);
    return false;
  end

  local world = cRoot:Get():GetWorld(self.worldName);

  -- Is it world found?
  if world == nil or world == false then
    return false;
  end

  local worldPosition = self.worldPosition;
  local furnaceItems = {};
  local furnaceCurrPos = {};
  local ret;
  local chestItemCurr;
  local chestSlot = 0;
  local InstanceBoundingBox  = cBoundingBox(Vector3d(self.mapCuboid.p1), Vector3d(self.mapCuboid.p2));

  -- Process per chest items
  for id1 in pairs(furnaces) do
    chestSlot = 0;
    furnaceCurr = furnaces[id1];

    furnaceCurrPos = Vector3i(worldPosition.x + furnaceCurr.x,
                            worldPosition.y + furnaceCurr.y,
                            worldPosition.z + furnaceCurr.z
    );

    -- Delete old chest items
    -- world:DoWithFurnaceAt(furnaceCurrPos.x, furnaceCurrPos.y, furnaceCurrPos.z,
    --   function(aFurnace)
    --     aFurnace:GetContents():Clear();
    --     return true;
    --   end
    -- );

    -- Dig block
    world:DigBlock(furnaceCurrPos.x, furnaceCurrPos.y, furnaceCurrPos.z);

    -- Set furnace block
    world:SetBlock(furnaceCurrPos.x, furnaceCurrPos.y, furnaceCurrPos.z, E_BLOCK_FURNACE, 2);

    -- Delete all entities into map cuboid
    world:ForEachEntityInBox(InstanceBoundingBox,
      function (aEntity)
        if aEntity:GetEntityType() == aEntity.etPickup then
          aEntity:Destroy();
        end
      end
    );

    -- Fill furnace
    world:DoWithFurnaceAt(furnaceCurrPos.x, furnaceCurrPos.y, furnaceCurrPos.z,
      function(aFurnace)

       aFurnace:SetFuelSlot(cItem(furnaceCurr.fuel_id, furnaceCurr.fuel_count, furnaceCurr.fuel_damage, furnaceCurr.fuel_enchant));

       aFurnace:SetInputSlot(cItem(furnaceCurr.input_id, furnaceCurr.input_count, furnaceCurr.input_damage, furnaceCurr.input_enchant));

       aFurnace:SetOutputSlot(cItem(furnaceCurr.output_id, furnaceCurr.output_count, furnaceCurr.output_damage, furnaceCurr.output_enchant));
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

