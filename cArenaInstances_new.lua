-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:new()
--
-------------------------------------------------------------------------------

function cArenaInstances:new()

  local object = {};

  object.mapID = 0;
  object.mapName = '';
  object.mapBlock = {}; -- cBlockArea loaded from DB
  object.mapSpawnPoints = {};
  object.mapCuboid = {};
  object.playersUUID = {};  -- List of players UUID request
  object.worldPosition = {}; -- Vector3i in world position (min point)
  object.secondsToStartRound = 10; -- Seconds to start round. TODO in gSystem:getRoundDelayTimer()
  object.startRound = false; -- Begin timer activate in global cron function
  object.playersLeftUUID = {}; -- List in game players
  -- object.playersLeft = 0; -- Number of players on the map. Depricated via playersLeftUUID
  object.worldName = '';
  object.arenaInstanceID = 0; -- Self ID in gArenaInstances[]

  -- Object create functions
  setmetatable(object, self);
  self.__index = self;

  return object;
end
-------------------------------------------------------------------------------

