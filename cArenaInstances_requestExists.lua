-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:requestExists()
--
-------------------------------------------------------------------------------
function cArenaInstances:requestExists(aUUID)
  local func_name = 'cArenaInstances:requestExists()';

  local playerUUIDExists = fasle;

  -- Process every record. In future - create new field with [UUID] = 1 for the fast check
  for id in pairs(self.playersUUID) do
    if self.playersUUID[id] == aUUID then
      playerUUIDExists = true;
      break;
    end
  end

  -- Return find status
  return playerUUIDExists;
end

