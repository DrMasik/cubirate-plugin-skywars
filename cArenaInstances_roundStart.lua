-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:raundStart()
--
-------------------------------------------------------------------------------

function cArenaInstances:raundStart(aWorld)

  local func_name = 'cArenaInstances:raundStart()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SendMessage(msgRoundStart);
      end
    );
  end

end
-------------------------------------------------------------------------------

