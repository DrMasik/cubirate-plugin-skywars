-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:saveExpirienceLevel()
--
-------------------------------------------------------------------------------

function cArenaInstances:saveExpirienceLevel()

  local func_name = 'cArenaInstances:saveExpirienceLevel()';

  local plUID;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        plUID = aPlayer1:GetUniqueID();
        gPlayers[plUID]:saveExpirienceLevel();
      end
    );
  end

end
-------------------------------------------------------------------------------

