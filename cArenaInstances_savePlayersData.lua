-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:savePlayersData()
--
-------------------------------------------------------------------------------

function cArenaInstances:savePlayersData()

  local func_name = 'cArenaInstances:savePlayersData()';

  local player;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        player = gPlayers[aPlayer1:GetUniqueID()];

        -- Save player inventory
        player:saveInventory();

        -- Save player health
        player:saveHealth();

        -- Save Food level
        player:saveFoodLevel();

        -- Save XP level
        player:saveExpirienceLevel();

        -- Set position
        player:setLastWorldName(aPlayer1:GetWorld():GetName());
        player:setLastPosition(Vector3d(aPlayer1:GetPosition()));

        -- Save position
        player:savePosition();

        -- Save game mode
        player:saveGameMode();

        -- Save players fly permission
        player:saveFlyPermission();

        -- Save players fying
        player:saveFlying();

        -- gSystem:console_log(aPlayer1:GetPosition().x, 1, func_name);
      end
    );
  end

end
-------------------------------------------------------------------------------

