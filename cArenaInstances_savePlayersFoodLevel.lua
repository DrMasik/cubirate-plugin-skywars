-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:savePlayersFoodLevel()
--
-------------------------------------------------------------------------------

function cArenaInstances:savePlayersFoodLevel()

  local func_name = 'cArenaInstances:savePlayersFoodLevel()';

  local plUID;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        plUID = aPlayer1:GetUniqueID();
        gPlayers[plUID]:saveFoodLevel();
      end
    );
  end

end
-------------------------------------------------------------------------------

