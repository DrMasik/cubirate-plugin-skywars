-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:savePlayersHealt()
--
-------------------------------------------------------------------------------

function cArenaInstances:savePlayersHealt()

  local func_name = 'cArenaInstances:savePlayersHealt()';

  local plUID;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        plUID = aPlayer1:GetUniqueID();
        gPlayers[plUID]:saveHealth();
      end
    );
  end

end
-------------------------------------------------------------------------------

