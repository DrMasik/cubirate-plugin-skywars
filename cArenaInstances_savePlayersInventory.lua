-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:savePlayersInventory()
--
-------------------------------------------------------------------------------

function cArenaInstances:savePlayersInventory()

  local func_name = 'cArenaInstances:savePlayersInventory()';

  local plUID;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        plUID = aPlayer1:GetUniqueID();
        gPlayers[plUID]:saveInventory();
      end
    );
  end

end
-------------------------------------------------------------------------------

