-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:savePlayersPosition()
--
-------------------------------------------------------------------------------

function cArenaInstances:savePlayersPosition()

  local func_name = 'cArenaInstances:savePlayersPosition()';

  local plUID;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        plUID = aPlayer1:GetUniqueID();
        gPlayers[plUID]:setLastWorldName(aPlayer1:GetWorld():GetName()); -- Return world
        gPlayers[plUID]:setLastPosition(Vector3d(aPlayer1:GetPosition())); -- Return position
      end
    );
  end

end
-------------------------------------------------------------------------------

