-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setMapBlock()
--
-------------------------------------------------------------------------------

function cArenaInstances:setMapBlock(aString)

  local block = cBlockArea();

  -- gSystem:console_log('Set block data', 1, 'cArenaInstances:setMapBlock()'); -- DEBUG

  if block:LoadFromSchematicString(aString) ~= true then
    self.mapBlock = nil
    -- gSystem:console_log('self.mapBlock = nil', 1, 'cArenaInstances:setMapBlock()'); -- DEBUG
  else
    self.mapBlock = block; -- cBlockArea loaded from DB
  end

end
-------------------------------------------------------------------------------

