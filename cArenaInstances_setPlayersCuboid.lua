-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersCuboid()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersCuboid()

  local func_name = 'cArenaInstances:setPlayersCuboid()';
  local mapSize = Vector3i(self.mapBlock:GetSize());

  local mapCuboid = cCuboid(
    Vector3i(self.worldPosition.x, self.worldPosition.y, self.worldPosition.z),
    Vector3i(self.worldPosition.x + mapSize.x, self.worldPosition.y + mapSize.y, self.worldPosition.z + mapSize.z)
  );

  -- Sort for better perfomance
  mapCuboid:Sort();

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        gPlayers[aPlayer1:GetUniqueID()]:setCuboid(mapCuboid);
      end
    );
  end

  -- Save map cuboid
  self.mapCuboid = mapCuboid;

  return true;
end
-------------------------------------------------------------------------------

-- -- Create map cuboid
  -- mapCuboid = cCuboid(
  --   Vector3i(mapPositionInworld.x, mapPositionInworld.y, mapPositionInworld.z),
  --   Vector3i(mapPositionInworld.x + mapSize.x, mapPositionInworld.y + mapSize.y, mapPositionInworld.z + mapSize.z)
  -- );

  -- -- Sort for better perfomance
  -- mapCuboid:Sort()

  -- -- Add map cuboid to player
  -- player:setCuboid(mapCuboid);

