-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersData()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersData()

  local func_name = 'cArenaInstances:setPlayersData()';

  local player;

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        player = gPlayers[aPlayer1:GetUniqueID()];

        -- Clean player inventory
        player:cleanInventory();

        -- Force player to survival mode
        aPlayer1:SetGameMode(gmSurvival);

        -- Limit player speed to 0
        aPlayer1:SetNormalMaxSpeed(0);
        aPlayer1:SetFlyingMaxSpeed(0);

        -- Disable fly
        aPlayer1:SetFlying(false);
        aPlayer1:SetCanFly(false);

        -- Set players expirience level to 0
        aPlayer1:SetCurrentExperience(0);

        -- Set players food level
        aPlayer1:SetFoodLevel(aPlayer1.MAX_FOOD_LEVEL);

        -- Set player's health full level
        aPlayer1:Heal(10);
      end
    );
  end

end
-------------------------------------------------------------------------------

