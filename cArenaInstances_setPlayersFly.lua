-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersFly()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersFly(aFlag)

  local func_name = 'cArenaInstances:setPlayersFly()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SetFlying(aFlag);
        aPlayer1:SetCanFly(aFlag);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

