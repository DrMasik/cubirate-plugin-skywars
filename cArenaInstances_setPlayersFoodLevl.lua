-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersFoodLevel()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersFoodLevel(aFlag)

  local func_name = 'cArenaInstances:setPlayersFoodLevel()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SetFoodLevel(aPlayer1.MAX_FOOD_LEVEL);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

