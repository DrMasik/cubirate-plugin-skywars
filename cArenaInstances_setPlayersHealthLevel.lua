-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersHealthLevel()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersHealthLevel(aFlag)

  local func_name = 'cArenaInstances:setPlayersHealthLevel()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:Heal(10);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

