-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersLeftUUID()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersLeftUUID(aUUID)

  local func_name = 'cArenaInstances:setPlayersLeftUUID()';

  self.playersLeftUUID[aUUID] = 1;

end
-------------------------------------------------------------------------------

