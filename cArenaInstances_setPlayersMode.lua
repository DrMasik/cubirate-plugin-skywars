-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersMode()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersMode(aMode)

  local func_name = 'cArenaInstances:setPlayersMode()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SetGameMode(aMode);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

