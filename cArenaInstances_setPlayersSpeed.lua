-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersSpeed()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersSpeed(aSpeed)

  local func_name = 'cArenaInstances:setPlayersSpeed()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SetNormalMaxSpeed(aSpeed);
        aPlayer1:SetFlyingMaxSpeed(aSpeed);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

