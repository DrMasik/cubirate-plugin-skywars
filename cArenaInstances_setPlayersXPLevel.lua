-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setPlayersXPLevel()
--
-------------------------------------------------------------------------------

function cArenaInstances:setPlayersXPLevel(aFlag)

  local func_name = 'cArenaInstances:setPlayersXPLevel()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SetCurrentExperience(0);
      end
    );
  end

  return true;
end
-------------------------------------------------------------------------------

