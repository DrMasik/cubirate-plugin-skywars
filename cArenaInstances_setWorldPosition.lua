-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:setWorldPosition()
--
-------------------------------------------------------------------------------

function cArenaInstances:setWorldPosition(aPoint)

  local func_name = 'cArenaInstances:setWorldPosition()';

  -- gSystem:console_log('aPoint = ('.. aPoint.x ..'; '.. aPoint.y ..'; '.. aPoint.z ..')', 1, func_name);

  self.worldPosition = aPoint;

end
-------------------------------------------------------------------------------

