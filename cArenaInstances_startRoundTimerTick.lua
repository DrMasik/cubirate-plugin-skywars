-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cArenaInstances:startRoundTimerTick()
--
-------------------------------------------------------------------------------
-- cRoot:Get():GetDefaultWorld():ScheduleTask(20 * secondsCount, cron);
function cArenaInstances:startRoundTimerTick(aWorld)

  local func_name = 'cArenaInstances:startRoundTimerTick()';

  for id in pairs(self.playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(self.playersUUID[id],
      function(aPlayer1)
        aPlayer1:SendMessage(msgRoundStartAt .. self.secondsToStartRound);
      end
    );
  end

  self.secondsToStartRound = self.secondsToStartRound - 1;

end
-------------------------------------------------------------------------------

