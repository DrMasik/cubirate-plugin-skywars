-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cLose:new()
--
-------------------------------------------------------------------------------

function cLose:new()
  local object = {};

  object.playerName = '';
  object.playerUUID = '';

  object.lastWorldName = ''; -- Last world from player was teleported game
  object.lastPosition = nil; -- Vector3i from player was teleported to game

  setmetatable(object, self);
  self.__index = self;

  return object;
end
-------------------------------------------------------------------------------

