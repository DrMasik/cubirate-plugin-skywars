-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cMapPositions:get()
--
-------------------------------------------------------------------------------

function cMapPositions:getPosition(aMapSize)

  local func_name = 'cMapPositions:getPosition()';

  local position = gSystem:getLimitPoint(); -- Return Vector3i position for the map into world coordinates
  local delimerSize = 160; -- The number of blocks between the maps. DOTO gSystem:getMapDelimerSize()

  if self.mapNextX == nil or self:getMapsCount() < 1 then
    self.mapNextX = gSystem:getLimitPoint().x;
  end

  position.x = self.mapNextX;

  self.mapNextX = self.mapNextX - aMapSize.x - delimerSize;

  gSystem:console_log('self.mapNextX = '.. self.mapNextX, 1, func_name);

  return true, position;
end
-------------------------------------------------------------------------------

