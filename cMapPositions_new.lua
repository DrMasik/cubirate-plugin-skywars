-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cMapPositions:new()
--
-------------------------------------------------------------------------------

function cMapPositions:new()

  local object = {};

  object.mapNextX = nil;
  object.mapsCount = 0; -- Number of active maps count

  -- Object create functions
  setmetatable(object, self);
  self.__index = self;

  return object;
end
-------------------------------------------------------------------------------

