-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cPlayers:calcPointMin()

  return Vector3i(
    math.min(self.point1.x, self.point2.x),
    math.min(self.point1.y, self.point2.y),
    math.min(self.point1.z, self.point2.z)
  );

end
-------------------------------------------------------------------------------

