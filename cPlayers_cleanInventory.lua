-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:cleanInventory()
--
-------------------------------------------------------------------------------

function cPlayers:cleanInventory()

  local func_name = 'cPlayers:cleanInventory()';

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      aPlayer:GetInventory():Clear();
      return true;
    end
  );

  return true;
end
-------------------------------------------------------------------------------

