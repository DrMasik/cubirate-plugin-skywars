-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:createBlock()
--
-------------------------------------------------------------------------------

function cPlayers:createBlock()
  local world = cRoot:Get():GetWorld(self.worldName);
  local block = cBlockArea();

  -- Check is it world success
  if not world then
    return false;
  end

  -- Try to read area from world
  if not block:Read(world, self.point1, self.point2, cBlockArea.baMetas + cBlockArea.baTypes) then
    return false;
  end

  self.blockArea = block;

  return true;
end
-------------------------------------------------------------------------------

