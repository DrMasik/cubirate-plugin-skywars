-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cPlayers:getBlockSchematicString()

  if self.blockArea == nil then
    return false;
  end

  return self.blockArea:SaveToSchematicString();

end
-------------------------------------------------------------------------------

