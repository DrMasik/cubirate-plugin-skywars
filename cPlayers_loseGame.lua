-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:loseGame()
--
-------------------------------------------------------------------------------

function cPlayers:loseGame()

  local func_name = 'cPlayers:loseGame()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      aPlayer:SendMessage(msgYouAreLose);
      aPlayer:GetInventory():Clear();
      player = aPlayer;
    end
  );

  -- Clean cuboid object
  self.mapCuboid = nil;

  -- Update decrease players count on map instance
  local arena = gArenaInstances[self.mapInstanceID];

  if arena ~= nil then
    arena:delPlayersLeftUUID(self.playerUUID);
  end

  -- arena:decreasePlayersLeft();

  -- Teleport player back
  -- self:teleportBack(); -- Use gLose[]

  -- Is it player object exists?
  if player == nil then
    return true;
  end

  local plUID = player:GetUniqueID();

  gLose[plUID] = cLose:new();
  gLose[plUID]:setPlayerUUID(self.playerUUID);

  -- Retore game mode
  self:restoreGameMode();

  -- Restore fly permission
  self:restoreFlyPermission();

  -- Restore flying
  self:restoreFlying();

  -- Restore player inventory
  self:restoreInventory();

  -- Restore health level
  self:restoreHealth();

  -- Restore food level
  self:restoreFoodLevel();

  -- Restore expirience
  self:restoreExpirienceLevel();

  -- Check winner
  if arena ~= nil then
    arena:checkWinner();
  end

end
-------------------------------------------------------------------------------

