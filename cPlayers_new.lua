-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:new()
--
-------------------------------------------------------------------------------

function cPlayers:new()
  local object = {};

  object.playerName = '';
  object.playerUUID = '';

  object.editMode = false;

  object.setChestAdd = false;

  object.point1 = {}; -- Vector3i
  object.point2 = {}; -- Vector3i
  object.pointMin = {}; -- Vector3i

  object.worldName = '';

  object.blockArea = nil;

  object.arenaName = '';
  object.arenaID = 0;

  object.spawnMark = false;

  -- Part for in game data
  object.mapCuboid = nil; -- Save map cuboid for map on player play
  object.mapInstanceID = 0; -- Map instance where player will playes (request send)

  object.lastWorldName = ''; -- Last world from player was teleported game
  object.lastPosition = nil; -- Vector3i from player was teleported to game

  setmetatable(object, self);
  self.__index = self;

  return object;
end
-------------------------------------------------------------------------------

