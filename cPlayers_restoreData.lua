-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreData()
--
-------------------------------------------------------------------------------

function cPlayers:restoreData()
-- Return codes

  local func_name = 'cPlayers:restoreData()';

  -- Retore game mode
  self:restoreGameMode();

  -- Restore position
  self:restorePosition();

  -- Restore fly permission
  self:restoreFlyPermission();

  -- Restore flying
  self:restoreFlying();

  -- Restore player inventory
  self:restoreInventory();

  -- Restore health level
  self:restoreHealth();

  -- Restore food level
  self:restoreFoodLevel();

  -- Restore expirience
  self:restoreExpirienceLevel();

  return true;
end
-------------------------------------------------------------------------------

