-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreExpirienceLevel()
--
-------------------------------------------------------------------------------

function cPlayers:restoreExpirienceLevel()
-- Return codes

  local func_name = 'cPlayers:restoreExpirienceLevel()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get expLevel
  local ret, expLevel = gStore:getExpirienceLevel(self.playerName);

  if ret == false then
    return false;
  end

  -- Set player's expLevel
  player:SetCurrentExperience(expLevel);

  -- Delete expLevel record from DB
  gStore:deleteExpirienceLevel(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

