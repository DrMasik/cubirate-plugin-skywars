-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreFlyPermission()
--
-------------------------------------------------------------------------------

function cPlayers:restoreFlyPermission()
-- Return codes

  local func_name = 'cPlayers:restoreFlyPermission()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get fly permission
  local ret, flyPerm = gStore:getFlyPermission(self.playerName);

  if ret == false or flyPerm == nil then
    return false;
  end

  -- Set player's flyPerm
  player:SetCanFly(flyPerm);

  -- Delete flyPerm record from DB
  gStore:deleteFlyPermission(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

