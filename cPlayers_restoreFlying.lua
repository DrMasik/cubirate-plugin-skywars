-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreFlying()
--
-------------------------------------------------------------------------------

function cPlayers:restoreFlying()
-- Return codes

  local func_name = 'cPlayers:restoreFlying()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get fly permission
  local ret, flying = gStore:getFlying(self.playerName);

  if ret == false or flying == nil then
    return false;
  end

  -- Set player's flying
  player:SetFlying(flying);

  -- Delete flying record from DB
  gStore:deleteFlying(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

