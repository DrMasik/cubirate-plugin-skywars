-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreFoodLevel()
--
-------------------------------------------------------------------------------

function cPlayers:restoreFoodLevel()
-- Return codes

  local func_name = 'cPlayers:restoreFoodLevel()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get foodLevel
  local ret, foodLevel = gStore:getFoodLevel(self.playerName);

  if ret == false or foodLevel <= 0 then
    return false;
  end

  -- Set player's foodLevel
  player:SetFoodLevel(foodLevel);

  -- Delete foodLevel record from DB
  gStore:deleteFoodLevel(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

