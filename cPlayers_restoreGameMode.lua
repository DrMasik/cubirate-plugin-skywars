-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreGameMode()
--
-------------------------------------------------------------------------------

function cPlayers:restoreGameMode()
-- Return codes

  local func_name = 'cPlayers:restoreGameMode()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get gameMode
  local ret, gameMode = gStore:getGameMode(self.playerName);

  if ret == false or gameMode == nil then
    return false;
  end

  -- Set player's gameMode
  player:SetGameMode(gameMode);

  -- Delete gameMode record from DB
  gStore:deleteGameMode(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

