-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreHealth()
--
-------------------------------------------------------------------------------

function cPlayers:restoreHealth()
-- Return codes

  local func_name = 'cPlayers:restoreHealth()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get health
  local ret, health = gStore:getHealth(self.playerName);

  if ret == false or health <= 0 then
    return false;
  end

  -- Set player's health
  player:SetHealth(health);

  -- Delete health record from DB
  gStore:deleteHealthForPlayer(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

