-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restoreInventory()
--
-------------------------------------------------------------------------------

function cPlayers:restoreInventory()
-- Return codes
-- 1 - Player has restored inventory
-- 2 - Error on restore inventory

  local func_name = 'cPlayers:restoreInventory()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Is it player inventory exists into DB
  if gStore:inventoryExists(self.playerName) == false then
    return true;
  end

  -- Get player grids
  local armorGrid     = player:GetInventory():GetArmorGrid(); -- cItemGrid
  local inventoryGrid = player:GetInventory():GetInventoryGrid(); -- cItemGrid
  local hotbarGrid    = player:GetInventory():GetHotbarGrid(); -- cItemGrid

  local items;
  local ret;
  local itemCurr;

  -- Load armor
  ret, items = gStore:getInventoryItems(self.playerName, 1);

  -- Is it not error - load inventory
  if ret == true then
    for id1 in pairs(items) do
      itemCurr = items[id1];

      armorGrid:SetSlot(itemCurr.slotID, cItem(itemCurr.itemID, itemCurr.item_count, itemCurr.item_damage, itemCurr.item_enchant, ''));
    end
  end

  -- Load inventory
  ret, items = gStore:getInventoryItems(self.playerName, 2);

  -- Is it not error - load inventory
  if ret == true then
    for id1 in pairs(items) do
      itemCurr = items[id1];

      inventoryGrid:SetSlot(itemCurr.slotID, cItem(itemCurr.itemID, itemCurr.item_count, itemCurr.item_damage, itemCurr.item_enchant, ''));
    end
  end

  -- Load hotbar
  ret, items = gStore:getInventoryItems(self.playerName, 3);

  -- Is it not error - load inventory
  if ret == true then
    for id1 in pairs(items) do
      itemCurr = items[id1];

      hotbarGrid:SetSlot(itemCurr.slotID, cItem(itemCurr.itemID, itemCurr.item_count, itemCurr.item_damage, itemCurr.item_enchant, ''));
    end
  end

  -- Clean enventory records from DB
  gStore:cleanInventoryForPlayer(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

