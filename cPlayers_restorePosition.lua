-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:restorePosition()
--
-------------------------------------------------------------------------------

function cPlayers:restorePosition()
-- Return codes

  local func_name = 'cPlayers:restorePosition()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Get plPosition
  local ret, plPosition = gStore:getPlayerPosition(self.playerName);

  if ret == false or plPosition == nil then
    return false;
  end

  -- Teleport player to world
  player:MoveToWorld(plPosition.worldName);

  -- Set player's position
  player:TeleportToCoords(
    plPosition.position.x,
    plPosition.position.y,
    plPosition.position.z
  );

  -- Delete plPosition record from DB
  gStore:deletePosition(self.playerName);

  return true;
end
-------------------------------------------------------------------------------

