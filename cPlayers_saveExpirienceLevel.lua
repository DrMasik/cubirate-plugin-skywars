-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveExpirienceLevel()
--
-------------------------------------------------------------------------------

function cPlayers:saveExpirienceLevel()
-- Return codes

  local func_name = 'cPlayers:saveExpirienceLevel()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deleteExpirienceLevel(self.playerName);

  -- Add health into DB
  gStore:insertExpirienceLevel(self.playerName, player:GetCurrentXp());

  return true;
end
-------------------------------------------------------------------------------

