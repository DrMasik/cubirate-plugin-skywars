-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveFlyPermission()
--
-------------------------------------------------------------------------------

function cPlayers:saveFlyPermission()
-- Return codes

  local func_name = 'cPlayers:saveFlyPermission()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  local canFly = 0;

  if player:CanFly() == true then
    canFly = 1;
  end

  -- Clean current data for the player
  gStore:deleteFlyPermission(self.playerName);

  -- Add health into DB
  gStore:insertFlyPermission(self.playerName, canFly);

  return true;
end
-------------------------------------------------------------------------------

