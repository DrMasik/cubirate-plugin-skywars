-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveFlying()
--
-------------------------------------------------------------------------------

function cPlayers:saveFlying()
-- Return codes

  local func_name = 'cPlayers:saveFlying()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deleteFlying(self.playerName);

  local isFlying = 0;
  if player:IsFlying() == true then
    isFlying = 1;
  end

  -- Add health into DB
  gStore:insertFlying(self.playerName, isFlying);

  return true;
end
-------------------------------------------------------------------------------

