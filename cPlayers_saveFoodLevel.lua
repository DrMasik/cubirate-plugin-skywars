-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveFoodLevel()
--
-------------------------------------------------------------------------------

function cPlayers:saveFoodLevel()
-- Return codes

  local func_name = 'cPlayers:saveFoodLevel()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deleteFoodLevel(self.playerName);

  -- Add health into DB
  gStore:insertFoodLevel(self.playerName, player:GetFoodLevel());

  return true;
end
-------------------------------------------------------------------------------

