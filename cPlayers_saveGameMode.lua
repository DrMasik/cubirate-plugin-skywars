-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveGameMode()
--
-------------------------------------------------------------------------------

function cPlayers:saveGameMode()
-- Return codes

  local func_name = 'cPlayers:saveGameMode()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deleteGameMode(self.playerName);

  -- Add health into DB
  gStore:insertGameMode(self.playerName, player:GetGameMode());

  return true;
end
-------------------------------------------------------------------------------

