-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveHealth()
--
-------------------------------------------------------------------------------

function cPlayers:saveHealth()
-- Return codes

  local func_name = 'cPlayers:saveHealth()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deleteHealth(self.playerName);

  -- Add health into DB
  gStore:insertHealth(self.playerName, player:GetHealth());

  return true;
end
-------------------------------------------------------------------------------

