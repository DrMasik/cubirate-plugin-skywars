-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:saveInventory()
--
-------------------------------------------------------------------------------

function cPlayers:saveInventory()
-- Return codes
-- 1 - Player has saved inventory
-- 2 - Error on save inventory

  local func_name = 'cPlayers:saveInventory()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Is it player inventory exists into DB TODO
  -- if gStore:inventoryExists(self.playerName) == true then
  --   return false, 1;
  -- end

  -- Save armor grid
  local armorGrid     = player:GetInventory():GetArmorGrid(); -- cItemGrid
  local inventoryGrid = player:GetInventory():GetInventoryGrid(); -- cItemGrid
  local hotbarGrid    = player:GetInventory():GetHotbarGrid(); -- cItemGrid

  local item; -- cItem

  -- Save armor grid
  local gridPos = armorGrid:GetFirstUsedSlot();

  while gridPos >= 0 do
    -- Get item
    item = armorGrid:GetSlot(gridPos);

    -- Save item into DB
    if gStore:inventorySave(self.playerName, item, 1, gridPos) == false then
      return false, 2;
    end

    -- Get next non empty slot
    gridPos = armorGrid:GetNextUsedSlot(gridPos);
  end

  -- Save inventory grid
  gridPos = inventoryGrid:GetFirstUsedSlot();
  while gridPos >= 0 do
    -- Get item
    item = inventoryGrid:GetSlot(gridPos);

    -- Save item into DB
    if gStore:inventorySave(self.playerName,item, 2, gridPos) == false then
      return false, 2;
    end

    -- Get next non empty slot
    gridPos = inventoryGrid:GetNextUsedSlot(gridPos);
  end

  -- Save hotbar grid
  gridPos = hotbarGrid:GetFirstUsedSlot();
  while gridPos >= 0 do
    -- Get item
    item = hotbarGrid:GetSlot(gridPos);

    -- Save item into DB
    if gStore:inventorySave(self.playerName,item, 3, gridPos) == false then
      return false, 2;
    end

    -- Get next non empty slot
    gridPos = hotbarGrid:GetNextUsedSlot(gridPos);
  end

  return true;
end
-------------------------------------------------------------------------------

