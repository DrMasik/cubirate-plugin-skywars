-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:savePosition()
--
-------------------------------------------------------------------------------

function cPlayers:savePosition()
-- Return codes

  local func_name = 'cPlayers:savePosition()';
  local player = nil;

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
      return true;
    end
  );

  -- Is it player found?
  if player == nil then
    return false;
  end

  -- Clean current data for the player
  gStore:deletePosition(self.playerName);

  -- Add health into DB
  gStore:insertPosition(self.playerName, player:GetWorld():GetName(), Vector3d(player:GetPosition()));

  return true;
end
-------------------------------------------------------------------------------

