-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:spawnMarkSave()
--
-------------------------------------------------------------------------------

function cPlayers:spawnMarkSave(aPoint)

  local func_name = 'cPlayers:spawnMarkSave()';

  gSystem:console_log('Begin function', 1, func_name); -- DEBUG

  -- Get arena name
  local arenaName = self:getArenaName();

  if arenaName == nil or arenaName == '' then
    gSystem:console_log('arenaName == nil', 1, func_name); -- DEBUG
    return false;
  end

  -- Get arena ID
  local arenaID = gStore:getArenaID(arenaName);

  if arenaID == nil or arenaID == false then
    gSystem:console_log('arenaID == nil', 1, func_name); -- DEBUG
    return false;
  end

  -- Is it arena border set?
  if self.point1 == nil or self.point2 == nil then
    -- aPlayer:SendMessageWarning(msgMarkPointsNotSet);
    gSystem:console_log('self.point1 == nil or self.point2 == nil', 1, func_name); -- DEBUG
    return false;
  end

  -- Check points mark
  local pointMin = self.pointMin;

  -- Is it point
  if pointMin == nil or pointMin == 0 then
    -- Calculate min point
    self.pointMin = self:calcPointMin();
    pointMin = self.pointMin;
  end

  -- Get self position
  local plPosition = aPoint;

  -- Calculate spawn position for insert into DB logical position
  local spawnPosition = Vector3i(math.abs(pointMin.x - plPosition.x), math.abs(pointMin.y - plPosition.y) + 2, math.abs(pointMin.z - plPosition.z));

  -- Add spawn point into DB
  local ret = gStore:insertSpawnPoint(arenaID, spawnPosition);

  if ret == false then
    gSystem:console_log('gStore:insertSpawnPoint() == nil', 1, func_name); -- DEBUG
    return false;
  end

  -- Show nice message
  -- aPlayer:SendMessageSuccess(msgSpawnSetSuccess);

  return true;
end
-------------------------------------------------------------------------------

