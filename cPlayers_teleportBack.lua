-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cPlayers:teleportBack()
--
-------------------------------------------------------------------------------

function cPlayers:teleportBack()
-- Teleport player to saved position
-- On player loose or win the game

  local func_name = 'cPlayers:teleportBack()';
  local player = nil;

  -- gSystem:console_log('Begin player teleport back', 1, func_name);

  -- Is it player was played?
  if self.lastWorldName == nil or self.lastWorldName == '' then
    gSystem:console_log('self.lastWorldName == nil', 2, func_name);
    return true;
  end

  local lastPosition = self.lastPosition;

  -- Is it position set?
  if lastPosition.x == nil or lastPosition.y == nil or lastPosition.z == nil then
    gSystem:console_log('lastPosition.x == nil or lastPosition.y == nil or lastPosition.z == nil', 2, func_name);
    return true;
  end

  cRoot:Get():DoWithPlayerByUUID(self.playerUUID,
    function(aPlayer)
      player = aPlayer;
    end
  );

  -- Is it player exists. May be dissconnect
  if player == nil then
    gSystem:console_log('Player object not found by self.playerUUID', 2, func_name);
    return true;
  end

  -- Get world object
  local world = cRoot:Get():GetWorld(self.lastWorldName);

  if world == nil then
    gSystem:console_log('World not found: "'.. self.lastWorldName ..'"', 2, func_name);
    return false;
  end

  -- Hook for local process
  local OnChunkAvailable = function() end

  -- After all chunks loaded - move player to last position
  local OnAllChunksAvailable = function()
    -- Move to world
    player:MoveToWorld(world);

    -- gSystem:console_log('Teleport player to ('.. lastPosition.x ..'; '.. lastPosition.y ..'; '.. lastPosition.z ..')', 1, func_name);

    -- Teleport to position
    player:TeleportToCoords(lastPosition.x, lastPosition.y, lastPosition.z);

    -- Delete on DB data
    gStore:deletePosition(self.playerName);
  end

  -- Force to load chunks
  world:ChunkStay({{lastPosition.x/16, lastPosition.z/16}}, OnChunkAvailable, OnAllChunksAvailable);

  -- world:ChunkStay({
  --     {math.floor(lastPosition.x/16), math.floor(lastPosition.z/16)},
  --     {lastPosition.x/16, lastPosition.z/16},
  --     {math.ceil(lastPosition.x/16), math.ceil(lastPosition.z/16)}
  --   },
  --   OnChunkAvailable, OnAllChunksAvailable
  -- );
end
-------------------------------------------------------------------------------

