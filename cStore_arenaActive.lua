-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:arenaActive()
--
-------------------------------------------------------------------------------

function cStore:arenaActive(aName)

  local func_name = 'cStore:arenaActive()';

  if aName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local name = aName;
  local arenaActive = false;

  -- Create SQL statement
  local sql = [=[
    SELECT id
    FROM arenas
    WHERE name like :name
          AND active = 1
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    name = name
  });

  -- Get arena data
  for data1 in stmt:urows() do
    arenaActive = true;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return arenaActive;

end
-------------------------------------------------------------------------------

