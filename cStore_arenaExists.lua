-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:arenaExists()
--
-------------------------------------------------------------------------------

function cStore:arenaExists(aName)

  local func_name = 'cStore:arenaExists()';

  if aName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local name = aName;
  local arenaExists = false;

  -- Create SQL statement
  local sql = [=[
    SELECT id
    FROM arenas
    WHERE name like :name
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    name = name
  });

  -- Get arena data
  for data1 in stmt:urows() do
    arenaExists = true;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return arenaExists;

end
-------------------------------------------------------------------------------

