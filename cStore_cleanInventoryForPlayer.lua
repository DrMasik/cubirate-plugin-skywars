-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:cleanInventoryForPlayer()
--
-------------------------------------------------------------------------------

function cStore:cleanInventoryForPlayer(aPlayerName)

  local func_name = 'cStore:cleanInventoryForPlayer()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  -- Create SQL statement
  local sql = [=[
    DELETE FROM inventories
    WHERE pl_name like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    gSystem:console_log(" -> ret code = ".. ret, 2, func_name);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true;
end
-------------------------------------------------------------------------------

