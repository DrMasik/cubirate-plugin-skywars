-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:deleteHealthForPlayer()
--
-------------------------------------------------------------------------------

function cStore:deleteHealthForPlayer(aPlayerName)

  local func_name = 'cStore:deleteHealthForPlayer()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  -- Create SQL statement
  local sql = [=[
    DELETE FROM health
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  stmt:step();

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true;
end
-------------------------------------------------------------------------------

