-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cStore:getBlock(aName)

  local func_name = 'cStore:getBlock()';

  if aName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local name = string.lower(aName);
  local data = nil;

  -- Create SQL statement
  local sql = [=[
    SELECT data
    FROM arenas
    WHERE name = :name
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    name = name
  });

  -- Insert record
  -- local ret = stmt:step();

  -- if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
  --   gSystem:console_log(" -> ret code = ".. ret, 2, func_name);
  --   stmt:finalize();
  --   db:close();
  --   return false;
  -- end

  -- Get arena data
  data = nil;
  for data1 in stmt:urows() do
    data = data1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return data;

end
-------------------------------------------------------------------------------

