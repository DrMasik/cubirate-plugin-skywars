-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getChestItems()
--
-------------------------------------------------------------------------------

function cStore:getChestItems(aChestID)

  local func_name = 'cStore:getChestItems()';

  if aChestID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local chestID = aChestID;

  -- Create SQL statement
  local sql = [=[
    SELECT item_id, item_count, item_enchant
    FROM items
    WHERE chest_id = :chestID
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    chestID =  chestID
  });

  local items ={};

  -- Get arena data
  for id1, count1, enchant1 in stmt:urows() do
    if enchant1 == nil then
      enchant1 = '';
    end

    items[id1] = {id = id1, count = count1, enchant = enchant1};
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, items;

end
-------------------------------------------------------------------------------

