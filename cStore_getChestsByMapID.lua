-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getChestsByMapID()
--
-------------------------------------------------------------------------------

function cStore:getChestsByMapID(aMapID)

  local func_name = 'cStore:getChestsByMapID()';

  if aMapID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local mapID = aMapID;
  local chests = {};

  -- Create SQL statement
  local sql = [=[
    SELECT id, x, y, z
    FROM chests
    WHERE arena_id = :mapID
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    mapID =  mapID
  });

  -- Get arena data
  for id1, x1, y1, z1 in stmt:urows() do
    chests[id1] = {id = id1, x = x1, y = y1, z = z1};
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, chests;

end
-------------------------------------------------------------------------------

