-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getFlyPermission()
--
-------------------------------------------------------------------------------

function cStore:getFlyPermission(aPlayerName)

  local func_name = 'cStore:getFlyPermission()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local permCode = nil;

  -- Create SQL statement
  local sql = [=[
    SELECT perm_code
    FROM players_fly_permission
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  for perm_code1 in stmt:urows() do
    permCode = perm_code1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  if permCode == 1 then
    permCode = true;
  elseif permCode == 0 then
    permCode = false;
  end

  return true, permCode;
end
-------------------------------------------------------------------------------

