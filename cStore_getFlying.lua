-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getFlying()
--
-------------------------------------------------------------------------------

function cStore:getFlying(aPlayerName)

  local func_name = 'cStore:getFlying()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local flyMode = nil;

  -- Create SQL statement
  local sql = [=[
    SELECT flying_code
    FROM players_flying
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  for flyMode1 in stmt:urows() do
    flyMode = flyMode1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  if flyMode == 0 then
    flyMode = false;
  else
    flyMode = true;
  end

  return true, flyMode;
end
-------------------------------------------------------------------------------

