-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getFoodLevel()
--
-------------------------------------------------------------------------------

function cStore:getFoodLevel(aPlayerName)

  local func_name = 'cStore:getFoodLevel()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local food = 0;

  -- Create SQL statement
  local sql = [=[
    SELECT food
    FROM food
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  for food1 in stmt:urows() do
    food = food1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, food;
end
-------------------------------------------------------------------------------

