-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getFurnacesByMapID()
--
-------------------------------------------------------------------------------

function cStore:getFurnacesByMapID(aMapID)

  local func_name = 'cStore:getFurnacesByMapID()';

  if aMapID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local mapID = aMapID;
  local furnaces = {};

  -- Create SQL statement
  local sql = [=[
    SELECT id, x, y, z,
      fuel_id,
      fuel_count,
      fuel_damage,
      fuel_enchant,

      input_id,
      input_count,
      input_damage,
      input_enchant,

      output_id,
      output_count,
      output_damage,
      output_enchant

    FROM furnaces
    WHERE arena_id = :mapID
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    mapID =  mapID
  });

  -- Get arena data
  for id1, x1, y1, z1,
      fuel_id1,
      fuel_count1,
      fuel_damage1,
      fuel_enchant1,

      input_id1,
      input_count1,
      input_damage1,
      input_enchant1,

      output_id1,
      output_count1,
      output_damage1,
      output_enchant1
    in stmt:urows() do

    furnaces[id1] = {id = id1, x = x1, y = y1, z = z1,
      fuel_id       = fuel_id1,
      fuel_count    = fuel_count1,
      fuel_damage   = fuel_damage1,
      fuel_enchant  = fuel_enchant1,

      input_id      = input_id1,
      input_count   = input_count1,
      input_damage  = input_damage1,
      input_enchant = input_enchant1,

      output_id       = output_id1,
      output_count    = output_count1,
      output_damage   = output_damage1,
      output_enchant  = output_enchant1
    };
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, furnaces;

end
-------------------------------------------------------------------------------

