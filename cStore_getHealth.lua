-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getHealth()
--
-------------------------------------------------------------------------------

function cStore:getHealth(aPlayerName)

  local func_name = 'cStore:getHealth()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local health = 0;

  -- Create SQL statement
  local sql = [=[
    SELECT health
    FROM health
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  for health1 in stmt:urows() do
    health = health1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, health;
end
-------------------------------------------------------------------------------

