-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getInventoryItems()
--
-------------------------------------------------------------------------------

function cStore:getInventoryItems(aPlayerName, aGridID)

  local func_name = 'cStore:getInventoryItems()';

  if aPlayerName == nil or aGridID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local items = {};

  -- Create SQL statement
  local sql = [=[
    SELECT grid_slot_number, item_code, item_count, item_damage, item_enchant
    FROM inventories
    WHERE pl_name like :plName
          AND grid_id = :gridID
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName,
    gridID = aGridID
  });

  for grid_slot_number, item_code, item_count, item_damage, item_enchant in stmt:urows() do
    items[#items + 1] = { slotID = grid_slot_number,
                      itemID = item_code,
                      item_count = item_count,
                      item_damage = item_damage,
                      item_enchant = item_enchant
    }
  end

  -- if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
  --   gSystem:console_log(" -> ret code = ".. ret, 2, func_name);
  --   stmt:finalize();
  --   db:close();
  --   return false;
  -- end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, items;
end
-------------------------------------------------------------------------------

