-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:getPlayerPosition()
--
-------------------------------------------------------------------------------

function cStore:getPlayerPosition(aPlayerName)

  local func_name = 'cStore:getPlayerPosition()';

  if aPlayerName == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local plPosition = nil;

  -- Create SQL statement
  local sql = [=[
    SELECT world_name, position_x, position_y, position_z
    FROM players_position
    WHERE login like :plName
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName
  });

  for worldName1, plPosition1_x, plPosition1_y, plPosition1_z in stmt:urows() do
    plPosition = {
      worldName = worldName1,
      position = Vector3d(plPosition1_x, plPosition1_y, plPosition1_z)
    };
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true, plPosition;
end
-------------------------------------------------------------------------------

