-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cStore:getSpawnPoints(aArenaID)

  local func_name = 'cStore:getSpawnPoints()';

  if aArenaID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local arenaID = aArenaID;
  local data = {};

  -- Create SQL statement
  local sql = [=[
    SELECT id
    FROM spwn_points
    WHERE arena_id = :arena_id
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    arena_id = arenaID
  });

  -- Get arena data
  data = nil;
  for data1 in stmt:urows() do
    data[#data + 1] = data1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return data;

end
-------------------------------------------------------------------------------

