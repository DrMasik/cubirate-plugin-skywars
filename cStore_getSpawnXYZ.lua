-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- getSpawnXYZ()
--
-------------------------------------------------------------------------------

function cStore:getSpawnXYZ(aArenaID)

  local func_name = 'cStore:getSpawnXYZ()';

  if aArenaID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local arenaID = aArenaID;
  local x, y, z;

  -- Create SQL statement
  local sql = [=[
    SELECT x, y, z
    FROM spwn_points
    WHERE arenaID = :arenaID
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    arenaID = arenaID
  });

  -- Get arena data
  for x1, y1, z1 in stmt:urows() do
    x = x1;
    y = y1;
    z = z1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return Vector3i(x, y, z);

end
-------------------------------------------------------------------------------

