-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cStore:init()

  local func_name = 'cStore:init()';

  local sql =[[
    ---------------------------------------------
    -- Table of arenas
    ---------------------------------------------

    -- DROP TABLE arenas;

    CREATE TABLE IF NOT EXISTS arenas(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT NOT NULL,
      players_number INT NOT NULL DEFAULT 0,
      active INT NOT NULL DEFAULT 0,
      data BLOB
    );

    CREATE UNIQUE INDEX IF NOT EXISTS arenas_id
    ON arenas(id);

    CREATE UNIQUE INDEX IF NOT EXISTS arenas_name
    ON arenas(name);

    ---------------------------------------------
    -- Create table for a chests
    ---------------------------------------------

    -- DROP TABLE IF EXISTS chests;

    CREATE TABLE IF NOT EXISTS chests(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      arena_id INTEGER,
      x INTEGER NOT NULL,
      y INTEGER NOT NULL,
      z INTEGER NOT NULL
    );

    CREATE UNIQUE INDEX IF NOT EXISTS chests_id
    ON chests(id);

    CREATE UNIQUE INDEX IF NOT EXISTS chests_arena_id_x_y_z
    ON chests(arena_id, x, y, z);

    ---------------------------------------------
    -- Create table for items
    ---------------------------------------------

    -- DROP TABLE IF EXISTS items;

    CREATE TABLE IF NOT EXISTS items(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      chest_id INT NOT NULL,
      item_id INT NOT NULL,
      item_count INT NOT NULL,
      item_enchant TEXT
    );

    CREATE UNIQUE INDEX IF NOT EXISTS items_id
    ON items(id);

    ---------------------------------------------
    -- Spawn points
    ---------------------------------------------

    -- DROP TABLE IF EXISTS spawn_points;

    CREATE TABLE IF NOT EXISTS spawn_points(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      arena_id INT NOT NULL,
      x INT NOT NULL,
      y INT NOT NULL,
      z INT NOT NULL
    );

    CREATE UNIQUE INDEX IF NOT EXISTS spawn_points_id
    ON spawn_points(id);

    CREATE UNIQUE INDEX IF NOT EXISTS spawn_points_arena_id_x_y_z
    ON spawn_points(arena_id, x, y, z);

    ---------------------------------------------
    -- Players inventory
    ---------------------------------------------

    -- DROP TABLE IF EXISTS inventories;

    CREATE TABLE IF NOT EXISTS inventories(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      pl_name TEXT NOT NULL,
      grid_id INT NOT NULL,
      grid_slot_number INT NOT NULL,
      item_code INT NOT NULL,
      item_count INT NOT NULL,
      item_damage INT NOT NULL DEFAULT 0,
      item_enchant TEXT NOT NULL DEFAULT ''
    );

    CREATE UNIQUE INDEX IF NOT EXISTS inventories_id
    ON inventories(id);

    CREATE INDEX IF NOT EXISTS inventories_pl_name
    ON inventories(pl_name);

    CREATE INDEX IF NOT EXISTS inventories_pl_name_grid_id
    ON inventories(pl_name, grid_id);

    ---------------------------------------------
    -- Save players health
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS health(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      health INT NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS health_id
    ON health(id);

    CREATE UNIQUE INDEX IF NOT EXISTS health_login
    ON health(login);

    ---------------------------------------------
    -- Save players food
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS food(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      food INT NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS food_id
    ON food(id);

    CREATE UNIQUE INDEX IF NOT EXISTS food_login
    ON food(login);

    ---------------------------------------------
    -- Save players expirience
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS expirience(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      expirience INT NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS expirience_id
    ON expirience(id);

    CREATE UNIQUE INDEX IF NOT EXISTS expirience_login
    ON expirience(login);

    ---------------------------------------------
    -- Create table for a furnace
    ---------------------------------------------

    -- DROP TABLE IF EXISTS furnaces;

    CREATE TABLE IF NOT EXISTS furnaces(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      arena_id INTEGER,
      x INTEGER NOT NULL,
      y INTEGER NOT NULL,
      z INTEGER NOT NULL,

      fuel_id INTEGER NOT NULL DEFAULT 0,
      fuel_count INTEGER NOT NULL DEFAULT 0,
      fuel_damage INTEGER NOT NULL DEFAULT 0,
      fuel_enchant TEXT NOT NULL DEFAULT '',

      input_id INTEGER NOT NULL DEFAULT 0,
      input_count INTEGER NOT NULL DEFAULT 0,
      input_damage INTEGER NOT NULL DEFAULT 0,
      input_enchant TEXT NOT NULL DEFAULT '',

      output_id INTEGER NOT NULL DEFAULT 0,
      output_count INTEGER NOT NULL DEFAULT 0,
      output_damage INTEGER NOT NULL DEFAULT 0,
      output_enchant TEXT NOT NULL DEFAULT ''
    );

    CREATE UNIQUE INDEX IF NOT EXISTS furnaces_id
    ON furnaces(id);

    CREATE UNIQUE INDEX IF NOT EXISTS furnaces_arena_id_x_y_z
    ON furnaces(arena_id, x, y, z);

    ---------------------------------------------
    -- Create table for save players game mode
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS players_gamemode(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      mode_code INTEGER NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS players_gamemode_id
    ON players_gamemode(id);

    CREATE UNIQUE INDEX IF NOT EXISTS players_gamemode_login
    ON players_gamemode(login);

    ---------------------------------------------
    -- Create table for save players fly permission
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS players_fly_permission(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      perm_code INTEGER NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS players_fly_permission_id
    ON players_fly_permission(id);

    CREATE UNIQUE INDEX IF NOT EXISTS players_fly_permission_login
    ON players_fly_permission(login);

    ---------------------------------------------
    -- Create table for save players fly permission
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS players_flying(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      flying_code INTEGER NOT NULL DEFAULT 0
    );

    CREATE UNIQUE INDEX IF NOT EXISTS players_flying_id
    ON players_flying(id);

    CREATE UNIQUE INDEX IF NOT EXISTS players_fly_permission_login
    ON players_flying(login);

    ---------------------------------------------
    -- Create table for save player's position
    ---------------------------------------------

    CREATE TABLE IF NOT EXISTS players_position(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL,
      world_name TEXT NOT NULL,
      position_x REAL NOT NULL,
      position_y REAL NOT NULL,
      position_z REAL NOT NULL
    );

    CREATE UNIQUE INDEX IF NOT EXISTS players_position_id
    ON players_position(id);

    CREATE UNIQUE INDEX IF NOT EXISTS players_fly_permission_login
    ON players_position(login);

  ]];

  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local ret = db:exec(sql);

  -- Is it allright?
  if ret ~= sqlite3.OK then
    gSystem:console_log(func_name .." -> db:exec return code ".. ret, 2);
    db:close();
    return false;
  end

  -- Close DB
  db:close();

  return true;

end
-------------------------------------------------------------------------------

