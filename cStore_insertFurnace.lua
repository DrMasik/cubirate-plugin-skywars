-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:insertFurnace()
--
-------------------------------------------------------------------------------

function cStore:insertFurnace(aArenaID, aPosition, aFuelItem, aInputInput, aOutputItem)

  local func_name = 'cStore:insertFurnace()';

  -- gSystem:console_log('Begin function', 1, func_name);

  if aArenaID == nil or aPosition == nil or aFuelItem == nil or aInputInput == nil or aOutputItem == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local arenaID   = aArenaID;
  local position  = aPosition;
  local fuel      = aFuelItem;
  local input     = aInputInput;
  local output    = aOutputItem;

  -- Create SQL statement
  local sql = [=[
    INSERT INTO furnaces(
      arena_id, x, y, z,
      fuel_id, fuel_count, fuel_damage, fuel_enchant,
      input_id, input_count, input_damage, input_enchant,
      output_id, output_count, output_damage, output_enchant
    )
    VALUES(
      :arenaID, :x, :y, :z,
      :fuelID, :fuelCount, :fuelDamage, :fuelEnchant,
      :inputID, :inputCount, :inputDamage, :inputEnchant,
      :outputID, :outputCount, :outputDamage, :outputEnchant
    )
    ;
  ]=];

  -- gSystem:console_log('Try to save furnace in position ('.. position.x ..'; '.. position.y ..'; '.. position.z ..')', 1, func_name);

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    arenaID = arenaID,

    x = position.x,
    y = position.y,
    z = position.z,

    fuelID      = fuel.m_ItemType,
    fuelCount   = fuel.m_ItemCount,
    fuelDamage  = fuel.m_ItemDamage,
    fuelEnchant = fuel.m_Enchantments:ToString(),

    inputID      = input.m_ItemType,
    inputCount   = input.m_ItemCount,
    inputDamage  = input.m_ItemDamage,
    inputEnchant = input.m_Enchantments:ToString(),

    outputID      = output.m_ItemType,
    outputCount   = output.m_ItemCount,
    outputDamage  = output.m_ItemDamage,
    outputEnchant = output.m_Enchantments:ToString()
  });

  -- Insert record
  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    gSystem:console_log(" -> ret code = ".. ret, 2, func_name);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true;

end
-------------------------------------------------------------------------------

