-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cStore:inventorySave(aPlayerName, aItemID, aGridID, aSlotID)

  local func_name = 'cStore:inventorySave()';

  if aPlayerName == nil or aItemID == nil or aGridID == nil or aSlotID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  -- Create SQL statement
  local sql = [=[
    INSERT INTO inventories(pl_name, grid_id, item_code, item_count, item_enchant, grid_slot_number, item_damage)
    VALUES(:plName, :gridID, :itemID, :count, :enchant, :slotID, :item_damage)
    ;
  ]=];

  -- Prepare item data
  local itemID = aItemID.m_ItemType;
  local itemCount = aItemID.m_ItemCount;
  local itemDamage = aItemID.m_ItemDamage;
  local itemEnchant = aItemID.m_Enchantments:ToString();
  local slotID = aSlotID;

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName  = aPlayerName,
    gridID  = aGridID,
    slotID  = slotID,
    itemID  = itemID,
    count   = itemCount,
    item_damage = itemDamage,
    enchant = itemEnchant
  });

  -- Insert record
  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    gSystem:console_log(" -> ret code = ".. ret, 2, func_name);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return true;

end
-------------------------------------------------------------------------------

