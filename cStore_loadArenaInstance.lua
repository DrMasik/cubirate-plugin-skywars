-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cStore:loadArenaInstance()
--
-------------------------------------------------------------------------------

function cStore:loadArenaInstance(aArenaID)

  local func_name = 'cStore:loadArenaInstance()';

  local arenaCurr;
  local arenaID = aArenaID;

  -- gSystem:console_log('Begin load arena from DB by arena id = '.. aArenaID, 1, func_name); -- DEBUG

  -- Create SQL statement
  local sql = [=[
    SELECT arenas.id, arenas.name, arenas.data, spawn_points.x, spawn_points.y, spawn_points.z
    FROM arenas, spawn_points
    WHERE arenas.id = :arenaID
          AND spawn_points.arena_id = arenas.id
          AND arenas.active = 1
    ;
  ]=];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    gSystem:console_log("Can not open DB ".. self.filePath, 2, func_name);
    return (-1);
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    gSystem:console_log("db:prepare is nil", 2, func_name);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    arenaID = arenaID
  });

  -- Create ne arena object
  arenaCurr = cArenaInstances:new();

  -- Get arena data
  for id1, name1, data1, x1, y1, z1 in stmt:urows() do

    -- gSystem:console_log('Load arena from DB with ID = '.. id1, 1, func_name); -- DEBUG

    arenaCurr:setMapID(id1);
    arenaCurr:setMapName(name1);
    arenaCurr:setMapBlock(data1);
    arenaCurr:addSpawnPoint(Vector3i(x1, y1, z1));

  end

  if arenaCurr:getMapID() == nil or arenaCurr:getMapID() == 0 then
    gSystem:console_log('arenaCurr:getMapID() == nil or arenaCurr:getMapID() == 0', 1, func_name);
    instanceID = 0;
  else

    --local tmp = 1;

    --for id1 in pairs(gArenaInstances) do
    --  tmp = id1;
    --end

    ---------------------------------------------------------
    local tmp = 1;

    -- gSystem:console_log('#gArenaInstances (tmp) = '.. tmp, 1, func_name);

    -- Save arena instance
    -- gArenaInstances[#gArenaInstances + 1] = arenaCurr;
    -- instanceID = #gArenaInstances;

    while gArenaInstances[tmp] ~= nil do
      -- gSystem:console_log('gArenaInstances['.. tmp ..'] ~= nil', 1, func_name);
      tmp = tmp + 1;
    end

    gArenaInstances[tmp] = arenaCurr;
    instanceID = tmp;

    -- gSystem:console_log('instanceID = '.. instanceID, 1, func_name);
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- return all right and instance ID
  return true, instanceID;

end
-------------------------------------------------------------------------------

