-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cSystem:chestSaveData(aPlUID)
  local func_name = 'cSystem:chestSaveData()';

  if aPlUID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local player      = gPlayers[aPlUID];
  local pointMin    = player:getPointMin(); -- Get logical (0; 0; 0)
  local block       = player:getBlock();

  if block:CountSpecificBlocks(E_BLOCK_CHEST) < 1 then
    return true, msgChestNotFoundAndNotProcesed;
  end

  local arenaLengthX, arenaLengthY, arenaLengthZ = block:GetCoordRange();
  local x, y, z;
  local chestContent;
  local i;
  local chestItemsCount;
  local chestID;

  local blockCurr = nil;
  local blockTypeCurr = nil;
  local world = cRoot:Get():GetWorld(player:getWorldName());
  local chestItems = cItems();
  local itemCurr;
  local arenaID = gStore:getArenaID(player:getArenaName());

  -- Delete all chests from DB for the arena
  if gStore:deleteChestsByArenaID(arenaID) == false then
    return false;
  end

  -- Find and save chest items
  x = 0;
  while x < arenaLengthX do

    z = 0;
    while z < arenaLengthZ do

      y = 0;
      while y < arenaLengthY do

          -- Process chest content
          world:DoWithChestAt(pointMin.x + x, pointMin.y + y, pointMin.z + z,
            function(aChest)
              -- Add chest to DB
              gStore:insertChest(arenaID, Vector3i(x, y, z));

              -- Get chest ID
              chestID = gStore:getChestID(arenaID, Vector3i(x, y, z));

              -- Delete all items from chest
              if gStore:deleteChestItems(chestID) == false then
                return false;
              end

              -- Get items into chest
              chestContent = aChest:GetContents();

              if chestContent == nil then
                -- aPlayer:SendMessageInfo(msgChestAreEmpty ..'('.. (pointMin.x + x) ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..')');
                return true, msgChestAreEmpty ..'('.. (pointMin.x + x) ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..')';
              end

              -- Copy items from chest
              chestContent:CopyToItems(chestItems);

              chestItemsCount = chestItems:Size();

              -- if chestItemsCount < 1 then
              --   aPlayer:SendMessageInfo(msgChestAreEmpty ..'('.. (pointMin.x + x) ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..')');
              -- end

              -- Process every item in chest
              i = 0;
              while i < chestItemsCount do
                itemCurr    = chestItems:Get(i);
                itemID      = itemCurr.m_ItemType;
                itemCount   = itemCurr.m_ItemCount;
                itemEnchant = itemCurr.m_Enchantments:ToString();

                -- aPlayer:SendMessage(itemID ..'; '.. itemCount ..'; '.. itemEnchant);

                -- Add item to DB
                if gStore:insertItem(chestID, itemID, itemCount, itemEnchant) == false then
                  -- aPlayer:SendMessageWarning(msgChestItemSavedIntoDBError ..' ('.. pointMin.x + x ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..')');
                end

                -- Next item in chest
                i = i + 1;
              end

              chestItems:Clear();

              return true;
            end
          );
        -- end
        y = y + 1;
      end -- for y

      z = z + 1;
    end -- for z

    x = x + 1;
  end -- for x

  return true;
end
-------------------------------------------------------------------------------

