-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cSystem:console_log(aStr, msgType, aFuncName)
-- 0 - standart
-- 1 - debug
-- 2 - error

  local str = '';

  if msgType == nil then
    msgType =0;
  end

  if msgType == 0 then
    str = aStr;
  elseif msgType == 1 and self.showConsoleDebugMessages == true then
      str = "Debug: ".. aStr;
  else
    str = "Error: ".. aStr;
  end

  if aFuncName ~= nil and aFuncName ~= '' then
    str = aFuncName ..' -> ' .. str;
  end

  if str ~= '' then
    LOG(self.pluginName ..": ".. str);
  end
end

-------------------------------------------------------------------------------

-- :%s/\s\+$//g
-- =============================================================================

