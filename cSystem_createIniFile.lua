-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cSystem:createIniFile(aDir, aFileName)
  local func_name = 'cSystem:createIniFile()';

  local IniFile = cIniFile();

  IniFile:AddValue("General", "showConsoleDebugMessages", 'true');
  IniFile:AddValue("General", "MapBorderPositionX", '2900000');
  IniFile:AddValue("General", "MapBorderPositionY", '254');
  IniFile:AddValue("General", "MapBorderPositionZ", '2900000');
  IniFile:AddValue("General", "instanceWorldName", cRoot:Get():GetDefaultWorld():GetName());
  IniFile:AddValue('General', 'PlayerReconnectDataRestoreTimeout', '3');

  if IniFile:WriteFile(aDir ..'/'.. aFileName) == false then
    return false;
  end

  return true;
end
--------------------------------------------------------------------------------

