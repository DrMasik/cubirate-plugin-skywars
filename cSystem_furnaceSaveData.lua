-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cSystem:furnaceSaveData()
--
-------------------------------------------------------------------------------

function cSystem:furnaceSaveData(aPlUID)
  local func_name = 'cSystem:furnaceSaveData()';

  -- gSystem:console_log('Begin function', 1, func_name);

  if aPlUID == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return false;
  end

  local player      = gPlayers[aPlUID];
  local pointMin    = player:getPointMin(); -- Get logical (0; 0; 0)
  local block       = player:getBlock();

  if block:CountSpecificBlocks(E_BLOCK_FURNACE) < 1 then
    return true, msgFurnaceNotFoundAndNotProcesed;
  end

  local arenaLengthX, arenaLengthY, arenaLengthZ = block:GetCoordRange();
  local x, y, z;
  local chestContent;
  local i;
  local chestItemsCount;
  local chestID;

  local blockCurr = nil;
  local blockTypeCurr = nil;
  local world = cRoot:Get():GetWorld(player:getWorldName());
  local chestItems = cItems();
  local itemCurr;
  local arenaID = gStore:getArenaID(player:getArenaName());

  -- Delete all furnaces from DB for the arena
  if gStore:deleteFurnaceByArenaID(arenaID) == false then
    -- gSystem:console_log('Delete furnace for arena ID '.. arenaID, 2, func_name);
    return false;
  end

  -- gSystem:console_log('arenaLength = ('.. arenaLengthX ..'; '.. arenaLengthY ..'; '.. arenaLengthZ ..')', 1, func_name);

  -- local count = 0;

  -- Find and save chest items
  x = 0;
  while x <= arenaLengthX do

    z = 0;
    while z <= arenaLengthZ do

      y = 0;
      while y <= arenaLengthY do

          -- local tmp = world:GetBlock(pointMin.x + x, pointMin.y + y, pointMin.z + z);-- DEBUG
          -- if tmp ~= E_BLOCK_AIR then
          --   gSystem:console_log('Block type in ('.. (pointMin.x + x) ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..') is '.. tmp, 1, func_name);
          -- end

          -- Process chest content
          world:DoWithFurnaceAt(pointMin.x + x, pointMin.y + y, pointMin.z + z,
            function(aFurnace)

              -- count = count + 1;

              -- gSystem:console_log('Process furance at ('.. (pointMin.x + x) ..'; '.. (pointMin.y + y) ..'; '.. (pointMin.z + z) ..')', 1, func_name);

              -- Get slot data
              local fuel = aFurnace:GetFuelSlot();
              local input = aFurnace:GetInputSlot();
              local output = aFurnace:GetOutputSlot();

              -- Add chest to DB
              gStore:insertFurnace(arenaID, Vector3i(x, y, z), fuel, input, output);

              -- if gStore:insertFurnace(arenaID, Vector3i(x, y, z), fuel, input, output) == true then
              --   gSystem:console_log('Furnace save success', 1, func_name);
              -- else
              --    gSystem:console_log('Furnace save error', 1, func_name);
              -- end

              return true;
            end
          );
        -- end
        y = y + 1;
      end -- for y

      z = z + 1;
    end -- for z

    x = x + 1;
  end -- for x

  -- gSystem:console_log('Total processed furances: '.. count, 1, func_name);

  return true;
end
-------------------------------------------------------------------------------

