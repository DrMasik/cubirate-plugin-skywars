-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cSystem:getInstanceIDFree()
--
-------------------------------------------------------------------------------

function cSystem:getInstanceIDFree(aArenaID)
-- Get map instance with empty slots

  local func_name = 'cSystem:getInstanceIDFree()';

  local instanceID = 0;
  local arenaInstance;

  -- Process every instanice
  for id in pairs(gArenaInstances) do
    arenaInstance = gArenaInstances[id];

    -- if arenaInstance:getMapID() == aArenaID and arenaInstance:getSpawnPointCount() < arenaInstance:getPlayersUUIDCount() then -- TODO:
    if arenaInstance:getMapID() == aArenaID and arenaInstance:isFull() == false then
      instanceID = id;
      break;
    end
  end

  return instanceID;
end
--------------------------------------------------------------------------------

