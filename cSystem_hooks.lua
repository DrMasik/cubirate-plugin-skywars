--
-- hooks()
--
--------------------------------------------------------------------------------

function cSystem:hooks()

  -- cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_LEFT_CLICK, OnPlayerLeftClick);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_JOINED,         OnPlayerJoined);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_SPAWNED,        OnPlayerSpawned);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_BREAKING_BLOCK, OnPlayerBreakingBlock);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_RIGHT_CLICK,    OnPlayerRightClick);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_MOVING,         OnPlayerMoving);
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_DESTROYED,      OnPlayerDestroyed);
  -- cPluginManager:AddHook(cPluginManager.HOOK_ENTITY_TELEPORT,       OnEntityTeleport);
  -- cPluginManager:AddHook(cPluginManager.HOOK_CHUNK_AVAILABLE,       OnChunkAvailable);

end
--------------------------------------------------------------------------------

-- function OnEntityTeleport(Entity, OldPosition, NewPosition)
--     func_name = 'OnEntityTeleport()';
--     gSystem:console_log('Begin function', 1, func_name);
-- end

-- function OnChunkAvailable(World, ChunkX, ChunkZ)
--   func_name = 'OnChunkAvailable()';
--   gSystem:console_log('Begin function', 1, func_name);
-- end

