-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function cSystem:loadPluginConfig(aDir, aFileName)
  local func_name = 'cSystem:loadPluginConfig()';

  local IniFile = cIniFile();

  if IniFile:ReadFile(aDir ..'/'.. aFileName) == false then
    return false;
  end

  self.showConsoleDebugMessages = IniFile:GetValue("General", "showConsoleDebugMessages");

  if self.showConsoleDebugMessages:lower() == 'true' then
    self.showConsoleDebugMessages = true;
  else
    self.showConsoleDebugMessages = false;
  end

  -- Get world name for map instances
  self.worldName = IniFile:GetValue("General", "instanceWorldName");

  -- Get world border (limit) point
  x = IniFile:GetValue("General", "MapBorderPositionX");
  y = IniFile:GetValue("General", "MapBorderPositionY");
  z = IniFile:GetValue("General", "MapBorderPositionZ");

  -- Load limit point
  self.limitPoint = Vector3i(x, y, z);

  return true;
end
--------------------------------------------------------------------------------

