--
-- cSystem:new()
--
-------------------------------------------------------------------------------

function cSystem:new()
  local object = {};

  object.pluginName = '';
  object.showConsoleDebugMessages = false;
  object.limitPoint = {}; -- Vector3i
  object.worldName = ''; -- World name for map instances
  object.PlayerReconnectDataRestoreTimeout = 3;

  setmetatable(object, self);

  self.__index = self;

  return object;
end
-------------------------------------------------------------------------------

