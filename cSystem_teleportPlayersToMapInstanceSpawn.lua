--
-- cSystem:teleportPlayersToMapInstanceSpawn()
--
-------------------------------------------------------------------------------

function cSystem:teleportPlayersToMapInstanceSpawn(aArenaInstanceID)
  local func_name = 'cSystem:teleportPlayersToMapInstanceSpawn()';

  -- Check income data
  if aArenaInstanceID == nil then
    return false;
  end

  local arenaInstanceID = aArenaInstanceID;
  local arena = gArenaInstances[arenaInstanceID];

  -- Is it arena instance exists?
  if arena == nil then
    return false;
  end

  local plID = 1;
  local spawnPoints = arena:getSpawnPoints();
  local spawnPoint;
  local playersUUID = arena:getPlayersUUID();
  local arenaWorldPosition = arena:getWorldPosition();
  local world = cRoot:Get():GetWorld(arena:getWorldName());

  if world == nil or world == false then
    gSystem:console_log('world == nil', 2, func_name);
    return false;
  end

  -- Process every player for the arena
  for id in pairs(spawnPoints) do
    -- Get spawn point coordinates
    spawnPoint = spawnPoints[id]; -- Vector3i

    -- Teleport player to spawn point
    cRoot:Get():DoWithPlayerByUUID(playersUUID[plID],
      function(aPlayer1)

        -- Teleport player to map world TODO
        aPlayer1:MoveToWorld(world);

        -- gSystem:console_log('Map logical 0 in World is: '.. arenaWorldPosition.x ..'; '.. arenaWorldPosition.y ..'; '.. arenaWorldPosition.z, 1, func_name);
        -- gSystem:console_log('Map logical spawn position is: '.. spawnPoint.x ..'; '.. spawnPoint.y ..'; '.. spawnPoint.z, 1, func_name);
        -- gSystem:console_log('Teleport player to '.. (arenaWorldPosition.x + spawnPoint.x) ..'; '.. (arenaWorldPosition.y + spawnPoint.y) ..'; '.. (arenaWorldPosition.z + spawnPoint.z), 1, func_name); -- DEBUG

        aPlayer1:TeleportToCoords(arenaWorldPosition.x + spawnPoint.x, arenaWorldPosition.y + spawnPoint.y, arenaWorldPosition.z + spawnPoint.z);
      end
    );

    -- Get ID next player
    plID = plID + 1;
  end

  return true;
end
-------------------------------------------------------------------------------

