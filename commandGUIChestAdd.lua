-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUIChestAdd(aSplit, aPlayer)

  local func_name = 'commandGUIChestAdd()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  gPlayers[aPlayer:GetUniqueID()]:setChestAdd(true);

  -- -- Show nice message
  aPlayer:SendMessageSuccess(msgChestAddmarkActivated);

  return true;
end

