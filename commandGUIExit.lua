-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUIExit(aSplit, aPlayer)
  local func_name = 'commandGUIExit()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  gPlayers[aPlayer:GetUniqueID()] = nil;

  -- Show nice message
  aPlayer:SendMessageSuccess(msgBuilderExit);

  return true;
end

