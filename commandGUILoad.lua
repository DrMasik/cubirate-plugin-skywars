-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUILoad(aSplit, aPlayer)
  local func_name = 'commandGUILoad()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it player set mark number
  if #aSplit < 3 then
    aPlayer:SendMessageWarning(msgLoadNameNotSet);
    return true;
  end

  -- local plUID = aPlayer:GetUniqueID();
  -- local player = gPlayers[plUID];
  local arenaName = aSplit[3];
  local schematicString = '';
  local block = cBlockArea();

  -- Get arena schematic string
  schematicString = gStore:getBlock(arenaName);

  -- Is it schematic string get from DB?
  if schematicString == nil or schematicString == false then
    aPlayer:SendMessageWarning(msgLoadBlockError);
    return true;
  end

  -- Load arena into memory
  if block:LoadFromSchematicString(schematicString) == false then
    aPlayer:SendMessageWarning(msgLoadSchematicStringError);
    return true;
  end

  -- local x, y, z = block:GetCoordRange();
  -- aPlayer:SendMessage('GetCoordRange() = '.. x ..'; '.. y ..'; '.. z);

  -- x, y, z = block:GetOrigin();
  -- aPlayer:SendMessage('GetOrigin() =' .. x ..'; '.. y ..'; '.. z);

  aPlayer:SendMessage(block:CountSpecificBlocks(54));

  if block:Write(aPlayer:GetWorld(), Vector3i(aPlayer:GetPosition()), cBlockArea.baTypes + cBlockArea.baMetas) == false then
    aPlayer:SendMessageWarning(msgLoadIntoWorldError);
    return true;
  end

  -- Show nice message
  aPlayer:SendMessageSuccess(msgLoadArenaSuccess);

  return true;
end
-------------------------------------------------------------------------------

