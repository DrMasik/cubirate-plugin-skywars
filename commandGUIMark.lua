-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUIMark(aSplit, aPlayer)
  local func_name = 'commandGUIMark()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it player set mark number
  if #aSplit < 3 then
    aPlayer:SendMessageWarning(msgMarkPointNumberNotSet);
    return true;
  end

  local plUID = aPlayer:GetUniqueID();
  local plPosition = Vector3i(aPlayer:GetPosition());
  local pointID = tonumber(aSplit[3]);

  -- Is it player class exists?
  if gPlayers[plUID] == nil then
    -- Create object
    gPlayers[plUID] = cPlayers:new();
    gPlayers[plUID]:setPlayerName(aPlayer:GetName());
    gPlayers[plUID]:setPlayerUUID(aPlayer:GetUUID());
  end

  -- Force activate edit mode
  gPlayers[plUID]:setEditMode(true);

  if pointID ~= 1 and pointID ~= 2 then
    aPlayer:SendMessageWarning(msgMarkPointNumberUnknown);
    return true;
  end

  -- Set point position
  gPlayers[plUID]:setPointByID(pointID, plPosition);

  -- Set world name
  gPlayers[plUID]:setWorldName(aPlayer:GetWorld():GetName());

  -- Show nice message
  aPlayer:SendMessageSuccess(msgMarkPointSet ..': ('.. plPosition.x ..'; '.. plPosition.y ..'; '.. plPosition.z ..')');

  return true;
end

