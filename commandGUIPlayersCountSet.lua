-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- commandGUIPlayersCountSet
--
-------------------------------------------------------------------------------

function commandGUIPlayersCountSet(aSplit, aPlayer)
  local func_name = 'commandGUIPlayersCountSet()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it player set mark number
  if #aSplit < 3 then
    aPlayer:SendMessageWarning(msgPlayersCountNotSet);
    return true;
  end

  local player = gPlayers[aPlayer:GetUniqueID()];
  local plNumber = tonumber(aSplit[3]);
  local arenaID;

  -- Is it player object exists?
  if player == nil then
    aPlayer:SendMessageWarning(msgSaveEditModNotActivated);
    return true;
  end

  -- Get arena ID
  arenaID = gStore:getArenaID(player:getArenaName());
  if arenaID == nil or arenaID == false then
    return false;
  end

  if gStore:setPlayersNumber(arenaID, plNumber) == false then
    return false;
  end

  -- Show players number set success
  aPlayer:SendMessageSuccess(msgPlayersNumberSetSuccess);

  return true;
end
-------------------------------------------------------------------------------

