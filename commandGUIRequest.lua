-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- commandGUIRequest()
--
-------------------------------------------------------------------------------

function commandGUIRequest(aSplit, aPlayer)
  local func_name = 'commandGUIRequest()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Request any map if no name set in future
  if #aSplit < 3 then
    aPlayer:SendMessageWarning(msgArenaNameNotSet);
    return true;
  end

  -- Set vars
  local arenaName = aSplit[3];
  local arenaID = 0;
  local arenaInstanceID = 0;
  local ret;
  local chunkToProcess = 0;
  local chunkProcessed = 0;
  local player = nil;

  -- Is it arena exists?
  if gStore:arenaExists(arenaName) == false then
    aPlayer:SendMessageWarning(msgArenaWrongName);
    return true;
  end

  -- Is it arena active?
  if gStore:arenaActive(arenaName) == false then
    aPlayer:SendMessageWarning(msgArenaTurnedOff);
    return true;
  end

  -- Get arena ID
  arenaID = gStore:getArenaID(arenaName);

  if arenaID == nil or arenaID == false or arenaID == 0 then
    aPlayer:SendMessageWarning(msgArenaWrongName);
    return true;
  end

  -- Get exists id of map instance with free slots
  arenaInstanceID = gSystem:getInstanceIDFree(arenaID);

  if arenaInstanceID == false then
    gSystem:console_log('Some error on get instance ID', 2, func_name); -- DEBUG

    -- Some error on get instance ID
    return false;
  elseif arenaInstanceID == 0 then
    -- gSystem:console_log('Load new map instance for arena ID = '.. arenaID, 1, func_name); -- DEBUG

    -- Load new map instance. All maps are beasy or map absent
    ret, arenaInstanceID = gStore:loadArenaInstance(arenaID);

   if ret == false then
     aPlayer:SendMessageWarning(msgMapLoadFalse ..' id = '.. arenaID);
     return true;
   end

    -- gSystem:console_log('arenaInstanceID = '.. arenaInstanceID, 1, func_name);

    -- Set arenaInstanceID
    gArenaInstances[arenaInstanceID]:setArenaInstanceID(arenaInstanceID);
  end

  -- Double check for error or not founded map instance
  if arenaInstanceID == false or arenaInstanceID == 0 then
    aPlayer:SendMessageWarning(msgMapGetinstanceErro);
    return true;
  end

  -- Get selected instance
  local arenaInstance = gArenaInstances[arenaInstanceID];

  -- Check player has request for the arena
  if arenaInstance:requestExists(aPlayer:GetUUID()) == true then
    aPlayer:SendMessageSuccess(msgRequestExists);
    return true;
  end

  -- Add player to map request
  -- arenaInstance:addPlayerdUUID(aPlayer:GetUUID());
  -- arenaInstance:setPlayersLeftUUID(aPlayer:GetUUID());
  -- arenaInstance:encreasePlayersLeft();

  -- Create player object for the arena instance
  local plUID = aPlayer:GetUniqueID();

  -- Is it player has request to other map?
  if gPlayers[plUID] ~= nil then
    -- Delete player from other map
    local mapInstanceID = gPlayers[plUID]:getMapInstanceID();

    -- Check to safe from crush on any errors
    if mapInstanceID ~= nil then
      gSystem:console_log('Clean map instance ID = '.. mapInstanceID, 1, func_name);

      -- for id1 in pairs(gArenaInstances) do
      --   gSystem:console_log('Listed arena instance ID = '.. id1, 1, func_name);
      -- end

      if gArenaInstances[mapInstanceID] ~= nil then
        gArenaInstances[mapInstanceID]:delPlayerUUID(aPlayer:GetUUID());
        gArenaInstances[mapInstanceID]:delPlayersLeftUUID(aPlayer:GetUUID());
      else
        gSystem:console_log('gArenaInstances[mapInstanceID] == nil', 1, func_name);
      end
    else
      gSystem:console_log('mapInstanceID == nil', 2, func_name);
    end
  end

  -- Add player to map request
  arenaInstance:addPlayerdUUID(aPlayer:GetUUID());
  arenaInstance:setPlayersLeftUUID(aPlayer:GetUUID());

  -- Recreate player object
  gPlayers[plUID] = nil;
  gPlayers[plUID] = cPlayers:new();
  gPlayers[plUID]:setPlayerName(aPlayer:GetName());
  gPlayers[plUID]:setPlayerUUID(aPlayer:GetUUID());

  gPlayers[plUID]:setMapInstanceID(arenaInstanceID);

  -- Check is it map full for start raund?
  if arenaInstance:isFull() == false then
    aPlayer:SendMessageSuccess(msgRequestPlayerAdded);
    return true;
  end

  -----------------------------------------------
  -- Map request is full. Prepare map, teleport players
  -- and begin round
  -----------------------------------------------
  -- gSystem:console_log("Arena are full. Begin map load", 1, func_name); -- DEBUG
  -- gSystem:console_log('Get instance size', 1, func_name); -- DEBUG

  local mapSize = arenaInstance:getInstanceSize();

  -- gSystem:console_log('Calculate minimum point on the map for the sky map', 1, func_name); -- DEBUG

  -- Get next position for the map into world
  local ret, mapPosition = gMapPositions:getPosition(mapSize);

  if ret == false then
    gSystem:console_log('gMapPositions:getPosition() == false', 1, func_name);
    return false;
  end

  -- Calculate minimum point on the map for the sky map
  local position = Vector3i(
    mapPosition.x - mapSize.x,
    mapPosition.y - mapSize.y,
    mapPosition.z - mapSize.z
  );

  -- local position = Vector3i(
  --   gSystem:getLimitPoint().x - mapSize.x,
  --   gSystem:getLimitPoint().y - mapSize.y,
  --   gSystem:getLimitPoint().z - mapSize.z
  -- );

  -- Calculate chunks count
  chunkToProcess = math.ceil((mapSize.x+32)/16) * math.ceil((mapSize.z+32)/16);

  -- gSystem:console_log('Chunks to process: '.. chunkToProcess, 1, func_name);
  -- gSystem:console_log('mapSize.x = '.. mapSize.x, 1, func_name);
  -- gSystem:console_log('mapSize.z = '.. mapSize.z, 1, func_name);

  -- Check is it one chunk
  if chunkToProcess < 1 then
    chunkToProcess = 1;
  end

  -- gSystem:console_log('Map size is ('.. mapSize.x ..'; '.. mapSize.y ..'; '.. mapSize.z ..')', 1, func_name);
  -- gSystem:console_log('Create map at ('.. gSystem:getLimitPoint().x ..'; '.. gSystem:getLimitPoint().y ..'; '.. gSystem:getLimitPoint().z ..')', 1, func_name);

  -- Save map position
  arenaInstance:setWorldPosition(position);

  -- aPlayer:SendMessage("Map will inserted into ".. position.x .."; ".. position.y ..'; '.. position.z); -- DEBUG

  -- Get world for maps instances
  local world = cRoot:Get():GetWorld(gSystem:getWorldName());

  -- Prepare chunk process functions
  local commandGUIRequestOnAllChunksAvaliable = function()

    -- Is it all chunks loaded?
    -- if chunkProcessed < chunkToProcess then
    --   return true;
    -- end

    -- gSystem:console_log(chunkProcessed ..' < '.. chunkToProcess, 1, func_name);

    -- gSystem:console_log('commandGUIRequestOnAllChunksAvaliable', 1, func_name); -- DEBUG

    -- Save players data
    arenaInstance:savePlayersData();

    -- Save players inventory to DB
    -- arenaInstance:savePlayersInventory();

    -- Save players health level
    -- arenaInstance:savePlayersHealt();

    -- Save players food level
    -- arenaInstance:savePlayersFoodLevel();

    -- Save players expirience level
    -- arenaInstance:saveExpirienceLevel();

    -- Save players game mode

    -- Save players fly permission

    -- Save players fying

    -- Clean player inventory
    -- arenaInstance:cleanPlayersInventory();

    -- Save player position
    -- arenaInstance:savePlayersPosition();

    -- Write down arena to world
    arenaInstance:getBlock():Write(world, position, cBlockArea.baTypes + cBlockArea.baMetas);

    -- Add on map maps number
    gMapPositions:increaseMapsCount();

    -- Set world for the instance
    arenaInstance:setWorldName(world:GetName());

    -- Set protected cuboid for the map
    arenaInstance:createCuboid();

    -- Create chests
    arenaInstance:loadChests();

    -- Create and load furance
    arenaInstance:loadFurnaces();

    -- Teleport player to spawn point
    if gSystem:teleportPlayersToMapInstanceSpawn(arenaInstanceID) == false then
      -- Some error
      gSystem:console_log('Error on player teleport to arena instance '.. arenaInstanceID, 2, func_name);
      return false;
    end

    -- Set players data
    arenaInstance:setPlayersData();

    -- Limit player speed to 0 (fly and normal)
    -- arenaInstance:setPlayersSpeed(0);

    -- Disable players fly
    -- arenaInstance:setPlayersFly(false);

    -- Force player to survival mode
    -- arenaInstance:setPlayersMode(gmSurvival);

    -- Set players expirience level to 0 :)
    -- arenaInstance:setPlayersXPLevel();

    -- Set players food level
    -- arenaInstance:setPlayersFoodLevel();

    -- Set player's health full level
    -- arenaInstance:setPlayersHealthLevel();

    -- Disable players to buid and break and use before round begin TODO:

    -- Set Cuboits for players
    arenaInstance:setPlayersCuboid();

    -- Begin timer to start round
    arenaInstance:setStartRound(true);
  end

  -- Empty chunk load hook
  -- local commandGUIRequestOnChunkAvailable = function()
  -- end

  -- Load chunk
  -- world:ChunkStay({{position.x/16, position.z/16}}, commandGUIRequestOnChunkAvailable, commandGUIRequestOnAllChunksAvaliable);

  -- Force load all chunks for the map
  local chunkX = 0;
  local chunkZ = 0;
  local chnkAddNumber = 32;
  local chunksArray = {};
  -- local chunkProcessed = 0;

  chunkX = 0;
  while chunkX <= mapSize.x + chnkAddNumber do

    chunkZ = 0;
    while chunkZ <= mapSize.z + chnkAddNumber do
      -- Chunk processed counter
      chunkProcessed = chunkProcessed + 1;

      table.insert(chunksArray, {(position.x + chunkX)/16, (position.z + chunkZ)/16})

      -- Load chunk
      -- world:ChunkStay({{(position.x + chunkX)/16, (position.z + chunkZ)/16}}, commandGUIRequestOnChunkAvailable, commandGUIRequestOnAllChunksAvaliable);

      -- Force chunk allways loaded until round is off
      -- world:SetChunkAlwaysTicked((position.x + chunkX)/16, (position.z + chunkZ)/16, true);

      -- Get next chunk
      chunkZ = chunkZ + 16;
    end
    chunkX = chunkX + 16;
  end

  world:ChunkStay(chunksArray, commandGUIRequestOnChunkAvailable, commandGUIRequestOnAllChunksAvaliable);

  -- gSystem:console_log('Total processed chunks: '.. chunkProcessed, 1, func_name);

  return true;
end
-------------------------------------------------------------------------------

-- :%s/\s\+$//g
-- =============================================================================

