-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUISave(aSplit, aPlayer)
  local func_name = 'commandGUISave()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it player set mark number
  if #aSplit < 3 then
    aPlayer:SendMessageWarning(msgSaveNameNotSet);
    return true;
  end

  local plUID = aPlayer:GetUniqueID();
  local player = gPlayers[plUID];
  local arenaName = aSplit[3];
  local schematicString = '';

  -- Is it player object exists?
  if player == nil then
    aPlayer:SendMessageWarning(msgSaveEditModNotActivated);
    return true;
  end

  -- Create block area
  if player:createBlock() == false then
    aPlayer:SendMessageWarning(msgSaveCanNotCreateBlock);
    return true;
  end

  -- Get string to save
  schematicString = player:getBlockSchematicString();

  -- Is it string correct?
  if schematicString == nil or schematicString == false then
    aPlayer:SendMessageWarning(msgSaveSchematicStringError);
    return true;
  end

  -- Add block to DB
  if gStore:saveBlock(arenaName, schematicString) == false then
    aPlayer:SendMessageWarning(msgSaveBlockError);
    return true;
  end

  -- Show nice message
  aPlayer:SendMessageSuccess(msgSaveArenaSuccess);

  -- Calculate and remember minimum point position
  player:setPointMin(player:calcPointMin());

  -- Save arena name
  player:setArenaName(arenaName);

  -- Begin process chests message to player
  aPlayer:SendMessageInfo(msgSaveProcessChests);

  -- Try to parse cuboid and save chest
  local ret, msg = gSystem:chestSaveData(plUID);

  -- Check ret message
  if ret == false then
    aPlayer:SendMessageWarning(msgSaveChestProcessError);
    return true;
  elseif ret == true and msg ~= nil then
    aPlayer:SendMessageInfo(msg);
  elseif ret == true then
    aPlayer:SendMessageSuccess(msgChestProcessedSuccess);
  end

  -- Prcess furnace
  ret, msg = gSystem:furnaceSaveData(plUID);
  if ret == false then
    aPlayer:SendMessageWarning(msgSaveFurnaceProcessError);
    return true;
  elseif ret == true and msg ~= nil then
    aPlayer:SendMessageInfo(msg);
  elseif ret == true then
    aPlayer:SendMessageSuccess(msgFurnaceProcessedSuccess);
  end

  return true;
end
-------------------------------------------------------------------------------

