-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUISpawnDel(aSplit, aPlayer)
  local func_name = 'commandGUISpawnDel()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it spawn ID set
  if #aSplit < 4 then
    return fasle;
  end

  local spawnID = tonumber(aSplit[4]);

  if spawnID == nil or spawnID == false then
    return false;
  end

  -- Try to delete point by ID
  if gStore:deleteSpawnPoint(spawnID) == false then
    return false;
  end

  aPlayer:SendMessageSuccess(msgSpawnDeleteSuccess);

  return true;
end

