-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUISpawnList(aSplit, aPlayer)
  local func_name = 'commandGUISpawnList()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  local player = gPlayers[aPlayer:GetUniqueID()];
  local arenaID;
  local arenaName;
  local spawnPoints;
  local message = cCompositeChat();

  -- Show separator
  aPlayer:SendMessage(msgStringDelimer);

  if player == nil then
    return false;
  end

  -- Get arena name
  arenaName = player:getArenaName();
  if arenaName == nil or arenaName == '' then
    return false;
  end

  -- Get arena ID
  arenaID = gStore:getArenaID(arenaName);
  if arenaID == nil or arenaID == false then
    return false;
  end

  -- Add spawn point into DB
  spawnPoints = gStore:getSpawnPoints(arenaID);
  if spawnPoints == nil or spawnPoints == false then
    return false;
  end

  for id1 in pairs(spawnPoints) do
    message
      :AddTextPart(id)
      :AddTextPart(' ')
      :AddRunCommandPart(msgSpawnDelete, '/SkyWars spawn del '.. id)
      :AddTextPart(' ')
      :AddRunCommandPart(msgSpawnTeleport, '/SkyWars spawn tp '.. id)
      :AddTextPart("\n")
    ;
  end

  -- Show nice message
  aPlayer:SendMessage(message);

  -- Show separator
  aPlayer:SendMessage(msgStringDelimer);

  return true;
end

