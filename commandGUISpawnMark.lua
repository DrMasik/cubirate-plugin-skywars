-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- commandGUISpawnMark()
--
-------------------------------------------------------------------------------

function commandGUISpawnMark(aSplit, aPlayer)
  local func_name = 'commandGUISpawnMark()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  gPlayers[aPlayer:GetUniqueID()]:setSpawnMark(true);

  return true;
end

