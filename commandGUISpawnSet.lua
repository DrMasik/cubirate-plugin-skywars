-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function commandGUISpawnSet(aSplit, aPlayer)
  local func_name = 'commandGUISpawnSet()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  local player = gPlayers[aPlayer:GetUniqueID()];
  local plPosition;
  local arenaID;
  local arenaName;
  local ret;

  if player == nil then
    return false;
  end

  -- Get arena name
  arenaName = player:getArenaName();
  if arenaName == nil or arenaName == '' then
    return false;
  end

  -- Get arena ID
  arenaID = gStore:getArenaID(arenaName);
  if arenaID == nil or arenaID == false then
    return false;
  end

  -- Is it arena border set?
  if player:getPoint1() == nil or player:getPoint2() == nil then
    aPlayer:SendMessageWarning(msgMarkPointsNotSet);
    return true;
  end

  -- Check points mark
  local pointMin = player:getPointMin();

  -- Is it point
  if pointMin == nil or pointMin == 0 then
    -- Calculate min point
    player:setPointMin(player:calcPointMin());
    pointMin = player:getPointMin();
  end

  -- Get player position
  plPosition = Vector3i(aPlayer:GetPosition());

  -- Calculate spawn position for insert into DB logical position
  local spawnPosition = Vector3i(math.abs(pointMin.x - plPosition.x) + 1, math.abs(pointMin.y - plPosition.y), math.abs(pointMin.z - plPosition.z) + 1);

  -- Add spawn point into DB
  ret = gStore:insertSpawnPoint(arenaID, spawnPosition);
  if ret == false then
    return false;
  end

  -- Show nice message
  aPlayer:SendMessageSuccess(msgSpawnSetSuccess);

  return true;
end

