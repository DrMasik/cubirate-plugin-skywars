-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- commandGUISpawnTP
--
-------------------------------------------------------------------------------

function commandGUISpawnTP(aSplit, aPlayer)
  local func_name = 'commandGUISpawnTP()';

  if aSplit == nil or aPlayer == nil then
    gSystem:console_log(msgFunctionArgumentsEmpty, 2, func_name);
    return true;
  end

  -- Is it spawn ID set
  if #aSplit < 4 then
    return fasle;
  end

  local spawnID = tonumber(aSplit[4]);

  if spawnID == nil or spawnID == false then
    return false;
  end

  local spawnPoint = gStore:getSpawnXYZ(spawnID);

  if spawnPoint == nil or spawnPoint == false then
    return false;
  end

  aPlayer:TeleportToCoords(spawnPoint.x, spawnPoint.y, spawnPoint.z);

  return true;
end

