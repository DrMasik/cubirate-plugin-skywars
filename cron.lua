-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cron()
--
-------------------------------------------------------------------------------
function cron()

  local func_name = 'cron()';

  -- Process every map instance
  for id in pairs(gArenaInstances) do
    -- gSystem:console_log('Process arena ID = '.. id, 1, func_name);

    arenaInstance = gArenaInstances[id];

    -- Is it rount start?
    if arenaInstance:getStartRound() == true then
      -- Show message to players
      if arenaInstance:getSecondsToStartRound() > 0 then
        arenaInstance:startRoundTimerTick();
      else
        -- Mark as round started, run round started functions
        arenaInstance:setStartRound(false);

        -- Run start function
        arenaInstance:raundStart();
        arenaInstance:setPlayersSpeed(1);
      end
    elseif arenaInstance:getPlayersLeftUUIDCount() < 1 then -- Is it empty arena?
      -- gSystem:console_log('Remove arena ID = '.. id, 1, func_name);

      -- gSystem:console_log('arenaInstance:getPlayersLeftUUIDCount() = '.. arenaInstance:getPlayersLeftUUIDCount(), 1, func_name);
      arenaInstance:cleanWorld();
      arenaInstance:Destroy();

      gMapPositions:decreaseMapsCount();
    end
  end

  -- Run cron in 1 second
  cRoot:Get():GetDefaultWorld():ScheduleTask(20, cron);

end
-------------------------------------------------------------------------------
