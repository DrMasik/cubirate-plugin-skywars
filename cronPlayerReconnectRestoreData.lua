-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cronPlayerReconnectRestoreData()
--
-------------------------------------------------------------------------------
function cronPlayerReconnectRestoreData()

  local func_name = 'cronPlayerReconnectRestoreData()';

  -- Process every map instance
  for id in pairs(gPlayersRestoreData) do

    gPlayersRestoreData[id]:restoreData();
    gPlayersRestoreData[id] = nil;

  end

  -- Run cronPlayerReconnectRestoreData in some delay
  cRoot:Get():GetDefaultWorld():ScheduleTask(
    20 * gSystem:getPlayerReconnectDataRestoreTimeout(),
    cronPlayerReconnectRestoreData
  );

end
-------------------------------------------------------------------------------
