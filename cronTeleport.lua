-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- cronTeleport()
--
-------------------------------------------------------------------------------
function cronTeleport()

  local func_name = 'cronTeleport()';

  -- Process every map instance
  for id in pairs(gLose) do

    gPlayers[id]:teleportBack();
    gLose[id] = nil;

  end

  -- Run cronTeleport in 1 second
  cRoot:Get():GetDefaultWorld():ScheduleTask(20, cronTeleport);

end
-------------------------------------------------------------------------------
