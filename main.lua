-- =============================================================================
--
-- SkyWars
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

-- gPluginName = '';
-- gRoomDefaultName = 'm-c.link';
-- gRankDBFilePath = '';
-------------------------------------------------------------------------------

function Initialize(Plugin)
  Plugin:SetName("SkyWars")
  Plugin:SetVersion(20160129)

  local pluginName = Plugin:GetName();
  local pluginDir = cRoot:Get():GetPluginManager():GetCurrentPlugin():GetLocalFolder();
  local func_name = 'Initialize()';

  -- Initialize global system class
  gSystem = cSystem:new();

  -- Save plugin name
  gSystem:setPluginName(pluginName);

  -- Check config file exists
  if cFile:IsFile(pluginDir ..'/config.ini') == false then
    -- File does not exists - create default
    gSystem:console_log('Config file does not exists. Create it.');

    gSystem:createIniFile(pluginDir, 'config.ini');
  end

  -- Load data from config ini file
  if gSystem:loadPluginConfig(pluginDir, 'config.ini') == false then
    return false;
  end

  -- Create nice message
  gSystem:console_log('-------------------------------------');
  gSystem:console_log(pluginName .. " initialize...")

  -- Load the InfoReg shared library:
  dofile(cPluginManager:GetPluginsPath() .. "/InfoReg.lua")

  -- Bind all the console commands:
  -- RegisterPluginInfoConsoleCommands()

  -- Bind all the commands (userspace):
  RegisterPluginInfoCommands()

  -- Load hooks
  gSystem:hooks();

  -- Init storage class
  gStore = cStore:new(pluginDir, 'sky_wars_maps.sqlite3');

  -- Create DB
  if gStore:init() == false then
    gSystem:console_log(msgStoreInitError, 2, func_name);
    return false;
  end

  -- Create map position calculator
  gMapPositions = cMapPositions:new();

  -- Activate plugin cron
  cron();
  cronTeleport();
  cronPlayerReconnectRestoreData();

  -- Nice message :)
  gSystem:console_log(pluginName ..": Initialized " .. pluginName .. " v." .. Plugin:GetVersion())
  gSystem:console_log('-------------------------------------');

  return true
end
-------------------------------------------------------------------------------

-- :%s/\s\+$//g
-- =============================================================================

