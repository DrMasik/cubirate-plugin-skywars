-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------
--
-- startGameTimer()
--
-------------------------------------------------------------------------------
-- cRoot:Get():GetDefaultWorld():ScheduleTask(20 * secondsCount, cron);
-- function startGameTimer(aArenaInstanceID)
function startGameTimer()

  local aArenaInstanceID = 1;
  local func_name = 'startGameTimer()';

  local arenaInstance = gArenaInstances[aArenaInstanceID];
  local playersUUID = arenaInstance:getPlayersUUID();
  local secondsToStartRound = arenaInstance:getSecondsToStartRound();

  for id in pairs(playersUUID) do
    cRoot:Get():DoWithPlayerByUUID(playersUUID[id],
      function(aPlayer1)
        aPlayer1:SendMessage(msgRoundStartAt .. secondsToStartRound);
      end
    );
  end

  secondsToStartRound = secondsToStartRound - 1;
  arenaInstance:setSecondsToStartRound(secondsToStartRound);

  if secondsToStartRound > 0 then
    cRoot:Get():GetDefaultWorld():ScheduleTask(20, startGameTimer);
  end

end
-------------------------------------------------------------------------------

